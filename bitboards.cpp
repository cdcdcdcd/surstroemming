#include <stdio.h>
#include "defs.hpp"

#define bits(bb) (__builtin_popcountll(bb))

int popAndGetLsb(U64* bb) { //same as pop
  int sq = lsb(*bb);
  popLsb(*bb);
  return sq;
}




//https://www.chessprogramming.org/BitScan
//matt taaylers folding trick
const int BitTable[64]  = {
    63, 30, 3, 32, 25, 41, 22, 33, 15, 50, 42, 13, 11, 53, 19, 34, 61, 29, 2,
    51, 21, 43, 45, 10, 18, 47, 1, 54, 9, 57, 0, 35, 62, 31, 40, 4, 49, 5, 52,
    26, 60, 6, 23, 44, 46, 27, 56, 16, 7, 39, 48, 24, 59, 14, 12, 55, 38, 28,
    58, 20, 37, 17, 36, 8
};


int POP(U64 *bb) {
    U64 b = *bb ^ (*bb -1);
    unsigned int fold = (unsigned) ((b & 0xffffffff) ^ (b >> 32));
    *bb &= (*bb - 1);
    return BitTable[(fold * 0x783a9b23) >> 26];
}

int CNT(U64 bb) {
    int c = 0;
    while(bb) {
        if(bb & 1)
            c++;
        bb = bb >> 1;
    }
    return c;
}

int HasNonPawn(S_BOARD* pos) {
  return bits(pos->occu[pos->side] ^ pos->piecebb[pos->side ? wk : bp] ^ pos->piecebb[pos->side ? wp : bp]);
}

void PrintBitBoard(U64 bb) {

    U64 shiftMe = 1ULL;

    int rank = 0;
    int file = 0;
    int sq = 0;
    int sq64 = 0;

    printf("\n");
    for(rank = RANK_8; rank >= RANK_1; --rank) {
        printf("%d", rank+1);
        for(file = FILE_A; file <= FILE_H; ++file) {
            sq64 = FR2SQ64(file,rank);

            if((shiftMe << sq64) & bb)
                printf("X");
            else
                printf("-");
        }
        printf("\n");
    }
    printf("\n\n");
}
