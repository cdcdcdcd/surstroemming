#include "defs.hpp"
#include <stdio.h>
// check ob das feld angegriffen wird
// recommended: ISP ISN etc check if piece is x

//int SqAttacked64(const int sq_in, const int side, const S_BOARD *pos){
//    int i, j, piece, isEnemy;
//    int sq2;
//    int sq = SQ120(sq_in);
//    // Knight
//    for(i = 0; i < 8; i++) {
//        sq2 = NDir[i] + sq;
//        if(ISOFFBOARD(sq2)){
//            continue;
//        }
//        piece = pos->pieces64[SQ64(sq2)];
//        isEnemy = colList[piece]^side;
//        if(isEnemy&&(piece==wn || piece==bn)) // TODO improve and merge 
//            return True;
//    }
//    // King
//    for(i = 0; i < 8; i++) {
//        sq2 = KDir[i] + sq;
//        if(ISOFFBOARD(sq2)){
//            continue;
//        }
//        piece = pos->pieces64[SQ64(sq2)];
//        isEnemy = colList[piece]^side;
//        if(isEnemy&&(piece==wk || piece==bk)) // TODO improve and merge 
//            return True;
//    }
//    // Rook
//    for(i = 0; i < 4; i++) {
//        for(j = 1; j < 8; j++) {
//            sq2 = RDir[i] * j + sq;
//            //falls offboard weiter machen
//            if(ISOFFBOARD(sq2)) // offbounds...
//                break; // break out
//            piece = pos->pieces64[SQ64(sq2)];
//            //falls figur guck ob gegnerischer turm oder dame sonst weiter machen
//            if(piece != EMPTY){
//                isEnemy = colList[piece]^side; 
//                if(isEnemy&&(piece == wr || piece == br || piece == wq || piece == bq))
//                    return True;
//                else // wenn die figur kein gegnerischer turm oder dame ist TODO assert
//                    break;
//            }
//        }
//    }
//
//    // Bishop
//    for(i = 0; i < 4; i++) {
//        for(j = 1; j < 8; j++) {
//            sq2 = BDir[i] * j + sq;
//            //falls offboard weiter machen
//            if(ISOFFBOARD(sq2))
//                break;
//            piece = pos->pieces64[SQ64(sq2)];
//            //falls figur guck ob gegnerischer turm oder dame sonst weiter machen
//            if(piece != EMPTY){
//                isEnemy = colList[piece]^side;
//                if(isEnemy && (piece == wb || piece == bb || piece == wq || piece == bq))
//                    return True;
//                else // wenn die figur kein gegnerischer läufer oder dame ist TODO assert
//                    break;
//            }
//        }
//    }
//    // pawn
//    // white -9 -11 black +
//    for(i = 0; i< 2 ; i++){ 
//        sq2 = sq+PDir[side][i]; //PDIR is only black pawn move but can be turned white with this multiplication
//        piece = pos->pieces64[SQ64(sq2)];
//        isEnemy = colList[piece]^side;
//        if(isEnemy && (piece == wp || piece == bp))
//            return True;
//    }    
//    return False;
//    
//}
