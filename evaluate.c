#include "stdio.h"
#include "defs.hpp"

const int PawnIsolated = -10;
const int PawnPassed[8] = {0,5,10,20,35,60,100,200};
const int RookOpenFile = 10;
const int RookSemiOpenFile = 5;

const int KingPrimaryPawnPenalty = -100;
const int KingSecondaryPawnPenalty = -40;

const int PawnTableO[64] = {
            0,   0,   0,   0,   0,   0,   0,   0,
           -31,   8,  -7, -37, -36, -14,   3, -31,
           -22,   9,   5, -11, -10,  -2,   3, -19,
           -26,   3,  10,   9,   6,   1,   0, -23,
           -17,  16,  -2,  15,  14,   0,  15, -13,
             7,  15,  10,  22,  22,  15,  22,   7,
            40,  44,  44,  35, 50,  44,  44,  44,
             0,   0,   0,   0,   0,   0,   0,   0
};

const int PawnTableE[64] = {
            0,   0,   0,   0,   0,   0,   0,   0,
           -31,   8,  -7, -37, -36, -14,   3, -31,
           -22,   9,   5, -11, -10,  -2,   3, -19,
           -26,   3,  10,   9,   6,   1,   0, -23,
           -17,  16,  -2,  15,  14,   0,  15, -13,
             7,  29,  21,  44,  40,  31,  44,   7,
            78,  83,  86,  73, 102,  82,  85,  90,
             0,   0,   0,   0,   0,   0,   0,   0
};


const int KnightTable[64] = {
           -74, -23, -26, -24, -19, -35, -22, -69,
           -23, -15,   2,   0,   2,   0, -23, -20,
           -18,  10,  13,  22,  18,  15,  11, -14,
            -1,   5,  31,  21,  22,  35,   2,   0,
            24,  24,  45,  37,  33,  41,  25,  17,
            10,  67,   1,  74,  73,  27,  62,  -2,
            -3,  -6, 100, -36,   4,  62,  -4, -14,
            -66, -53, -7, -75, -10, -55, -58, -70
};

const int BishopTable[64] = {
            -7,   2, -15, -12, -14, -15, -10, -10,
            19,  20,  11,   6,   7,   6,  20,  16,
            14,  25,  24,  15,   8,  25,  20,  15,
            13,  10,  17,  23,  17,  16,   0,   7,
            25,  17,  20,  34,  26,  25,  15,  10,
            -9,  39, -32,  41,  52, -10,  28, -14,
           -11,  20,  35, -42, -39,  31,   2, -22,
            -59, -78, -82, -76, -23,-107, -37, -50
};

const int RookTable[64] = { //TODO exaggerated
           -30, -24, -18,   5,  -2, -18, -31, -32,
           -53, -38, -31, -26, -29, -43, -44, -53,
           -42, -28, -42, -25, -25, -35, -26, -46,
           -28, -35, -16, -21, -13, -29, -46, -30,
             0,   5,  16,  13,  18,  -4,  -9,  -6,
            19,  35,  28,  33,  45,  27,  25,  15,
            55,  29,  56,  67,  55,  62,  34,  60,
            35,  29,  33,   4,  37,  33,  56,  50
};

const int QueenTable[64] = {
           -39, -30, -31, -13, -31, -36, -34, -42,
           -36, -18,   0, -19, -15, -15, -21, -38,
           -30,  -6, -13, -11, -16, -11, -16, -27,
           -14, -15,  -2,  -5,  -1, -10, -20, -22,
             1, -16,  22,  17,  25,  20, -13,  -6,
            -2,  43,  32,  60,  72,  63,  43,   2,
            14,  32,  60, -10,  20,  76,  57,  24,
            6,   1,  -8,-104,  69,  24,  88,  26
};

const int KingTableO[64] = {
            17,  30,  -3, -14,   6,  -1,  40,  18,
            -4,   3, -14, -50, -57, -18,  13,   4,
           -47, -42, -43, -79, -64, -32, -29, -32,
           -55, -43, -52, -28, -51, -47,  -8, -50,
           -55,  50,  11,  -4, -19,  13,   0, -49,
           -62,  12, -57,  44, -67,  28,  37, -31,
           -32,  10,  55,  56,  56,  55,  10,   3,
            4,  54,  47, -99, -99,  60,  83, -62
};

const int KingTableE[64] = {
-20	,	-15	,	-5	,	-5	,	-5	,	-5	,	-15	,	-20	,
-15	,	-10	,	0	,	0	,	0	,	0	,	-10	,	-15	,
-5	,	0	,	5	,	8	,	8	,	5	,    0	,	-5	,
-5	,	0	,	8	,	10	,	10	,	8	,	0	,	-5	,
-5	,	0	,	8	,	10	,	10	,	8	,	0	,	-5	,
-5	,	0	,	5	,	8	,	8	,	5	,	0	,	-5	,
-15	,	-10	,	0	,	0	,	0	,	0	,	-10	,	-15	,
-20	,	-15	,	-5	,	-5	,	-5	,	-5	,	-15	,	-20		
};

const int Mirror64[64] = {
56	,	57	,	58	,	59	,	60	,	61	,	62	,	63	,
48	,	49	,	50	,	51	,	52	,	53	,	54	,	55	,
40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,
32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,
24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,
16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,
8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,
0	,	1	,	2	,	3	,	4	,	5	,	6	,	7
};

int MaterialDraw(const S_BOARD *pos) {
    if(pos->allpawns != 0)
        return False;
    //NO pawns from here
    int wbwsq = 0;
    int wbbsq = 0;
    int bbwsq = 0;
    int bbbsq = 0;

    int wnvar = 0;
    int bnvar = 0;

    if(pos->piecebb[wq] != 0 ||pos->piecebb[bq] != 0||pos->piecebb[wr] != 0 ||pos->piecebb[br] != 0) return False;
    //NO rook, no queen from here
    if(pos->piecebb[wn] != 0){
        wnvar = CNT(pos->piecebb[wn]);
        if(wnvar >= 3) return False;
    }
    if(pos->piecebb[bn] != 0){
        bnvar = CNT(pos->piecebb[bn]);
        if(bnvar >= 3) return False;
    }
    if(pos->piecebb[wb] != 0){
        U64 bishops = pos->piecebb[wb];
        while(bishops){
            int sq = popAndGetLsb(&bishops);
            if(sq % 2 == 0) wbbsq++;
            else            wbbsq++;
        }
    }
    if(pos->piecebb[bb] != 0){
        U64 bishops = pos->piecebb[bb];
        while(bishops){
            int sq = popAndGetLsb(&bishops);
            if(sq % 2 == 0) bbbsq++;
            else            bbwsq++;
        }
    }
    int bbvar = bbbsq + bbwsq;
    int wbvar = wbbsq + wbwsq;

    //zero or one or two knights against bare king
    if(wnvar < 3 && wbvar == 0 && bbvar == 0 && bnvar == 0) return True;
    if(bnvar < 3 && wbvar == 0 && bbvar == 0 && wnvar == 0) return True;

    //two bishops draw against a bishop
    if(wbvar == 2 && wnvar == 0 && bbvar == 1 && bnvar == 0) return True;
    if(bbvar == 2 && wnvar == 0 && wbvar == 1 && bnvar == 0) return True;
    //both sides have bishop of same color
    if(wbvar == 1 && bbvar == 1 && wbwsq == bbwsq) return True; //needs only check wbwsq == bbwsq because other col is implied bc num is 1

    //two minor pieces against one draw, except when bisop pair
    int wbpair = wbwsq && wbbsq;
    int bbpair = bbwsq && bbbsq;
    int wminor = wbvar + wnvar;
    int bminor = wbvar + wnvar;
    if(!wbpair && wminor == 2 && bminor == 1) return True;
    if(!bbpair && bminor == 2 && wminor == 1) return True;

    return True;
}

int IsRepetition(S_BOARD* pos){
    for (int i = pos->hisPly - pos->fiftyMove; i < pos->hisPly-1; ++i) {
       if(pos->posKey == pos->history[i].posKey){
           return True;
       } 
    }

    return False;
}

int CheckIfOver(S_BOARD* pos){
    if(MaterialDraw(pos)||pos->fiftyMove >= 100) {
        return DRAW;
    }
    S_MOVELIST list;  //init an empty list
    list.count = 0;
    GenerateAllMoves(&list,pos);
    int MoveNum = 0;
    for(MoveNum = 0;MoveNum < list.count;++MoveNum){
        S_MOVE move = list.moves[MoveNum];
        if(MakeMove(pos,move.move)){
            UndoMove(pos); 
            return False;
        }
        UndoMove(pos);
    }

    int k = pos->side ? bk : wk;
    int incheck = BitSqAttacked(lsb(pos->piecebb[k]),pos->side,pos);
    if(incheck){
        return CHECKMATED; 
    }else{
        return DRAW;
    }
}

int EvalPosition(const S_BOARD *pos) {

	int pce;
	int pceNum;
	int sq;
	int score = pos->material[WHITE] - pos->material[BLACK];
    int side = pos->side;
	
    for(pce = wp; pce <= bk; pce++){
        U64 piecebb = pos->piecebb[pce];
        while(piecebb){
            sq = popAndGetLsb(&piecebb);
            switch (pce) {
                case wk:
                    if(pos->material[BLACK] < ENDGAMEMATERIAL) {
                        score += KingTableE[sq]; //if its endgame
                    }
                    else {
                        if(!(RankMask[sq] & pos->piecebb[wp])){//no pawn in front TODO front
                            score += KingPrimaryPawnPenalty;
                        }
                        if(!(RankMask[sq+E] & pos->piecebb[wp]) && sq+E < 64 && sq + E >= 0){//no pawn in front TODO front
                            score += KingSecondaryPawnPenalty;
                        }
                        if(!(RankMask[sq+W] & pos->piecebb[wp]) && sq+W < 64 && sq + W >= 0){//no pawn in front TODO front
                            score += KingSecondaryPawnPenalty;
                        }
                        score += KingTableO[sq]; 
                    }
                    break; 
                case wp:
                    if(pos->material[BLACK] < ENDGAMEMATERIAL) {
                        score += PawnTableE[sq]; 
                    }
                    else {
                        score += PawnTableO[sq]; 
                    }
                    if(!(IsolatedMask[sq] & pos->piecebb[wp])){
                        score += PawnIsolated;
                    }
                    if(!(PassedMask[sq][WHITE] & pos->piecebb[wp])){
                        score += PawnPassed[RANKSQ64(sq)];
                    }
                    break; 
                case wn:
                    score += KnightTable[sq]; 
                    break; 
                case wr:
                    score += RookTable[sq]; 
                    if(!(pos->allpawns & RankMask[sq])){
                        score += RookOpenFile;
                    } 
                    if(!(pos->piecebb[wp] & RankMask[sq])){
                        score += RookSemiOpenFile;
                    } 
                    break; 
                case wb:
                    score += BishopTable[sq]; 
                    break; 
                case wq:
                    score += QueenTable[sq]; 
                    break; 

                //everything here is a -
                case bk:

                    if(pos->material[WHITE] < ENDGAMEMATERIAL) {
                        score -= KingTableE[sq];  //if its endgame
                    }
                    else {
                        if(!(RankMask[sq] & pos->piecebb[bp])){//no pawn in front TODO front
                            score -= KingPrimaryPawnPenalty;
                        }
                        if(!(RankMask[sq+E] & pos->piecebb[bp]) && sq+E < 64 && sq + E >= 0){//no pawn in front TODO front
                            score -= KingSecondaryPawnPenalty;
                        }
                        if(!(RankMask[sq+W] & pos->piecebb[bp]) && sq+W < 64 && sq + W >= 0){//no pawn in front TODO front
                            score -= KingSecondaryPawnPenalty;
                        }
                        score -= KingTableO[Mirror64[sq]]; 
                    }
                    break; 
                case bp:
                    if(pos->material[WHITE] < ENDGAMEMATERIAL) {
                        score -= PawnTableE[Mirror64[sq]]; 
                    }
                    else {
                        score -= PawnTableO[Mirror64[sq]]; 
                    }
                    if(!(IsolatedMask[sq] & pos->piecebb[bp])){
                        score -= PawnIsolated;
                    }
                    if(!(PassedMask[sq][BLACK] & pos->piecebb[bp])){
                        score -= PawnPassed[7-RANKSQ64(sq)];
                    }
                    break;
                case bn:
                    score -= KnightTable[Mirror64[sq]]; 
                    break;
                case br:
                    score -= RookTable[Mirror64[sq]]; 
                    if(!(pos->allpawns & RankMask[sq])){
                        score -= RookOpenFile;
                    } 
                    if(!(pos->piecebb[bp] & RankMask[sq])){
                        score -= RookSemiOpenFile;
                    } 
                    break;
                case bb:
                    score -= BishopTable[Mirror64[sq]]; 
                    break; 
                case bq:
                    score -= QueenTable[Mirror64[sq]]; 
                    break; 
                default:
                    break;
            }
        }
    }
	
	if(side == WHITE) {
		return score;
	} else {
		return -score;
	}	
}

int MoveExists(S_BOARD *pos, const int move) {
	
    S_MOVELIST list;  //init an empty list
    list.count = 0;
    GenerateAllMoves(&list,pos);
      
    int MoveNum = 0;
	for(MoveNum = 0; MoveNum < list.count; ++MoveNum) {	
        S_MOVE lemove = list.moves[MoveNum];
       
        if ( !MakeMove(pos,lemove.move))  {
            UndoMove(pos);
            continue;
        }        
        UndoMove(pos);
		if(list.moves[MoveNum].move == move) {
			return True;
		}
    }
	return False;
}


//TODO delete
//int _CheckIfOver(S_BOARD* pos){
//    S_MOVELIST list;  //init an empty list
//    list.count = 0;
//    GenAllMoves(pos,&list);
//    int MoveNum = 0;
//    for(MoveNum = 0;MoveNum < list.count;++MoveNum){
//        S_MOVE move = list.moves[MoveNum];
//        if(MakeMove(pos,move.move)){
//            UndoMove(pos); 
//            return False;
//        }
//        UndoMove(pos);
//    }
//
//    int k = pos->side ? bk : wk;
//    //int incheck = SqAttacked(pos->pList[k][0],pos->side,pos);
//    int incheck = B
//    if(incheck){
//        return CHECKMATED; 
//    }else{
//        return DRAW;
//    }
//}
