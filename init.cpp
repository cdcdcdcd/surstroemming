#include "defs.hpp"
#include "stdlib.h"
#include "stdio.h"

#define RAND_64 	((U64)rand() | \
					(U64)rand() << 15 | \
					(U64)rand() << 30 | \
					(U64)rand() << 45 | \
					((U64)rand() & 0xf) << 60 )  


U64 PieceKeys[13][64]; //SQ_num
U64 SideKey;
U64 CastleKeys[16];

//https://www.chessprogramming.org/Classical_Approach
U64 rayAttacks[8][64]; 

U64 KnightAttacks[64];
U64 KingAttacks[64];

U64 PawnAttacks[2][64];
U64 PawnQuiet[2][64];
U64 PawnBlock[2][64];

U64 WKCbb = (1ULL << f1) | (1ULL << g1) ;
U64 WQCbb = (1ULL << b1) | (1ULL << c1) | (1ULL << d1) ;
U64 BKCbb = (1ULL << f8) | (1ULL << g8) ;
U64 BQCbb = (1ULL << b8) | (1ULL << c8) | (1ULL << d8) ;

U64 PassedMask[64][2];
/* 11100000
 * 11100000
 * 11100000
 * 11100000
 * 0P000000
 * 00000000
 * 00000000
 * 00000000
 * */

U64 IsolatedMask[64];
/* 10100000
 * 10100000
 * 10100000
 * 1P100000
 * 10100000
 * 10100000
 * 10100000
 * 10100000
 * */

U64 RankMask[64];
/* 10000000
 * 10000000
 * 10000000
 * 10000000
 * 10000000
 * 10000000
 * 10000000
 * 10000000
 * */
U64 FileMask[64];
/* 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 11111111
 * */

const int dirs[8] = {NE,N,NW,E,W,SE,S,SW}; //TODO put in initpawns

int abs(int x){
    return x < 0 ? -x : x;
}

int sqdist64(int sq64_1, int sq64_2){
    //int olddist = abs(FILESQ(SQ120(sq64_1))-FILESQ(SQ120(sq64_2)))+abs(RANKSQ(SQ120(sq64_1))-RANKSQ(SQ120(sq64_2)));
    int dist = abs(FILESQ64(sq64_1)-FILESQ64(sq64_2))+abs(RANKSQ64(sq64_1)-RANKSQ64(sq64_2));
    return dist;
}

void InitPawnMask(){

    int dir;
    int sq;
    int sqindex;
    //INIT pawn quiet
    for(int r = RANK_2; r < RANK_8; r++){ //doesnt include RANK_8 or 2
        for(int f = FILE_A; f <= FILE_H; f++){
            sqindex = FR2SQ64(f,r);
            PawnQuiet[WHITE][sqindex] |= (1ULL << (sqindex+N));
            PawnQuiet[BLACK][sqindex] |= (1ULL << (sqindex+S));
            PawnBlock[WHITE][sqindex] |= (1ULL << (sqindex+N));
            PawnBlock[BLACK][sqindex] |= (1ULL << (sqindex+S));
            if(r == RANK_2) 
                PawnQuiet[WHITE][sqindex] |= (1ULL << (sqindex+N+N));
            if(r == RANK_7) 
                PawnQuiet[BLACK][sqindex] |= (1ULL << (sqindex+S+S));
        }
    }
    

    //PrintBitBoard(PawnQuiet[BLACK][a2]);

    //INIT pawn attacks
    for(int r = RANK_1; r <= RANK_8; r++){
        for(int f = FILE_A; f <= FILE_H; f++){
            sqindex = FR2SQ64(f,r);
            if(sqdist64(sqindex,sqindex+N+E) <= 2)
                PawnAttacks[WHITE][sqindex] |= (1ULL << (sqindex+N+E));
            if(sqdist64(sqindex,sqindex+N+W) <= 2)
                PawnAttacks[WHITE][sqindex] |= (1ULL << (sqindex+N+W));
            if(sqdist64(sqindex,sqindex+S+E) <= 2)
                PawnAttacks[BLACK][sqindex] |= (1ULL << (sqindex+S+E));
            if(sqdist64(sqindex,sqindex+S+W) <= 2)
                PawnAttacks[BLACK][sqindex] |= (1ULL << (sqindex+S+W));
        }
    }

    //PrintBitBoard(PawnAttacks[BLACK][a2]);
    //INIT knightattacks
    //INIT kingattacks
    int knightdirs[8] = {N+N+E,N+N+W,S+S+E,S+S+W,E+E+N,E+E+S,W+W+N,W+W+S};
    int kingdirs[8] = {N,W,S,E,NE,NW,SE,SW};
    for(int sqindex = 0; sqindex < 64; sqindex++){
        for(int j = 0; j < 8; j++){

            sq = sqindex + knightdirs[j]; 
            if(sqdist64(sq,sqindex) <= 3 && sq >= 0 && sq < 64)
                KnightAttacks[sqindex] |= (1ULL << sq);

            sq = sqindex + kingdirs[j]; 
            if(sqdist64(sq,sqindex) <= 2 && sq >= 0 && sq < 64)
                KingAttacks[sqindex] |= (1ULL << sq);
        }
    }


    //printf("abstand %d\n", sqdist64(e2,d8));
    //PrintBitBoard(KnightAttacks[d8]);

    //INIT rayattacks
    for(int i = 0;i<8;i++){
        dir = dirs[i]; 
        for(int sqindex= 0;sqindex<64;sqindex++){
            sq = sqindex;
            while(sq < 64 && sq >= 0){
                //if(sqdist64(sq,sq+dir)>2){
                if(sq+dir < 0 || sq+dir >= 64 || sqdist64(sq,sq+dir) > 2){
                    break;
                }
                sq += dir;
                rayAttacks[i][sqindex] |= (1ULL << sq);
            }
        }
    }

    //PrintBitBoard(rayAttacks[1][(SQ64(D4))]);
    //printf("msb %d\n", msb(rayAttacks[1][(SQ64(D4))]));
    //popLsb(rayAttacks[1][(SQ64(D4))]);
    //PrintBitBoard(rayAttacks[1][(SQ64(D4))]);
        
    //INIT passedmask
    for(int r = RANK_1; r < RANK_8; r++){ //doesnt include RANK_8
        for(int f = FILE_A; f <= FILE_H; f++){
            sqindex = FR2SQ64(f,r);
            for(int ro = 1; r+ro <= RANK_8; ro++){
                sq = FR2SQ64(f,r+ro); //in front
                PassedMask[sqindex][WHITE] |= (1ULL << sq);
                if(f != FILE_A){
                    sq = FR2SQ64(f-1,r+ro); //in front
                    PassedMask[sqindex][WHITE] |= (1ULL << sq);
                }
                if(f != FILE_G){
                    sq = FR2SQ64(f+1,r+ro); //in front
                    PassedMask[sqindex][WHITE] |= (1ULL << sq);
                }
            }
        }
    }

    for(int r = RANK_1; r < RANK_8; r++){ //doesnt include RANK_8
        for(int f = FILE_A; f <= FILE_H; f++){
            sqindex = FR2SQ64(f,r);
            for(int ro = 1; r-ro >= RANK_1; ro++){
                sq = FR2SQ64(f,r-ro); //in front
                PassedMask[sqindex][BLACK] |= (1ULL << sq);
                if(f != FILE_A){
                    sq = FR2SQ64(f-1,r-ro); //in front
                    PassedMask[sqindex][BLACK] |= (1ULL << sq);
                }
                if(f != FILE_G){
                    sq = FR2SQ64(f+1,r-ro); //in front
                    PassedMask[sqindex][BLACK] |= (1ULL << sq);
                }
            }
        }
    }
    //PrintBitBoard(PassedMask[SQ64(A3)][BLACK]);
    
    //INIT Passmask
    for(int r = RANK_1; r <= RANK_8; r++){ 
        for(int f = FILE_A; f <= FILE_H; f++){
            sqindex = FR2SQ64(f,r);
            for(int ro = RANK_1; ro <= RANK_8; ro++){
                if(f != FILE_A){
                    sq = FR2SQ64(f-1,ro); //in front
                    IsolatedMask[sqindex] |= (1ULL << sq);
                }
                if(f != FILE_G){
                    sq = FR2SQ64(f+1,ro); //in front
                    IsolatedMask[sqindex] |= (1ULL << sq);
                }
            }
        }
    }
    //PrintBitBoard(IsolatedMask[SQ64(B3)]);

    //INIT Rankmask
    for(int r = RANK_1; r <= RANK_8; r++){ 
        for(int f = FILE_A; f <= FILE_H; f++){
            sqindex = FR2SQ64(f,r);
            for(int ro = RANK_1; ro <= RANK_8; ro++){
                sq = FR2SQ64(f,ro); //in front
                RankMask[sqindex] |= (1ULL << sq);
            }
        }
    }
    //PrintBitBoard(RankMask[SQ64(B3)]);
    //INIT filemask
    for(int r = RANK_1; r <= RANK_8; r++){ 
        for(int f = FILE_A; f <= FILE_H; f++){
            sqindex = FR2SQ64(f,r);
            for(int fo = FILE_A; fo <= FILE_H; fo++){
                sq = FR2SQ64(fo,r); //in front
                FileMask[sqindex] |= (1ULL << sq);
            }
        }
    }
    //PrintBitBoard(FileMask[b3]);
}

void InitHashKeys() {
	
	int index = 0;
	int index2 = 0;
	for(index = 0; index < 13; ++index) {
		for(index2 = 0; index2 < 64; ++index2) {
			PieceKeys[index][index2] = RAND_64;
		}
	}
	SideKey = RAND_64;
	for(index = 0; index < 16; ++index) {
		CastleKeys[index] = RAND_64;
	}

}


int Sq120ToSq64[BRD_SQ_NUM];
int Sq64ToSq120[64];

U64 SetMask[64];
U64 ClearMask[64];

void InitBitMasks() {
    int index = 0;

    for(index = 0; index < 64; index++){
        SetMask[index] = 0ULL;
        ClearMask[index] = 0ULL;
    }

    for(index = 0; index < 64; index++){
        SetMask[index] |= (1ULL << index);
        ClearMask[index] = ~SetMask[index];
    }
}

void InitSq120To64() {

    int index = 0;
    int file = FILE_A;
    int rank = RANK_1;
    int sq = A1;
    int sq64 = 0;
    for(index = 0; index < BRD_SQ_NUM; ++index) {
        Sq120ToSq64[index] = 65;
    }

    for(index = 0; index < 64; ++index) {
        Sq64ToSq120[index] = 120;
    }

    for(rank = RANK_1; rank <= RANK_8; ++rank) {
        for(file = FILE_A; file <= FILE_H; ++file) {
            sq = FR2SQ(file,rank);
            Sq64ToSq120[sq64] = sq;
            Sq120ToSq64[sq] = sq64;
            sq64++;
        }
    }
}

void AllInit(){
    InitSq120To64();
    InitBitMasks();
    InitMvvLva();
    InitPawnMask();
    InitHashKeys();
    InitPolyBook();
}
