#include <stdio.h>
#include "defs.hpp"
int CheckBoardNew(S_BOARD* pos){
    for(int pce = wp; pce <= bk; pce++){
        U64 bb = pos->piecebb[pce];
        while(bb){
            int sq = popAndGetLsb(&bb);
            if(pos->pieces64[sq] != pce){
                return False;
            }
        }
    }

    for(int i = 0; i < 64; i++){
        int piece = pos->pieces64[i];
        if(piece != EMPTY){
            if(!(pos->piecebb[piece] & (1ULL << i))){
                return False;
            }
        }
    }

    return True;
}

void UpdatePieceLists(S_BOARD *pos) {
    
    int piece, sq, index, col;
    for(index = 0; index < 64; ++index) {
        sq = index;
        piece = pos->pieces64[index];
        col = colList[piece];
        if(piece != EMPTY){
            //if(isBig[piece]) pos->bigNum[col]++;
            //if(isMaj[piece]) pos->majNum[col]++;
            //if(isMin[piece]) pos->minNum[col]++;
            pos->material[col] += valList[piece];
            
            //pos->pList[piece][pos->pceNum[piece]] = sq;
            //pos->pceNum[piece]++;

            if(piece == wp || piece == bp) SETBIT(pos->allpawns, sq);
            SETBIT(pos->piecebb[piece], sq);
            SETBIT(pos->occu[colList[piece]],sq);
            SETBIT(pos->occu[BOTH],sq);
        }
    }
}

void printfen(S_BOARD *pos){
    int i = 0; //empty square counter
    char piece; 
    for(int j = 0;j<64;j++){
        piece = printpce[pos->pieces64[(63-7+(j%8)-(j/8)*8)]];
        if(j%8==0&&j!=0&&j!=64){
            if(i){
                printf("%d",i);
            }
            i = 0;
            printf("/");
        }
        if(piece == '.'){
            i++;
        }
        else {
            if(i){
                printf("%d",i);
            }
            i = 0;
            printf("%c",piece);
        }
    }
    printf(" %c",pos->side ? 'b' : 'w');
    int K = pos->castlePerm & 0x1;
    int Q = pos->castlePerm >> 1 & 0x1;
    int k = pos->castlePerm >> 2 & 0x1;
    int q = pos->castlePerm >> 3 & 0x1;
    printf(" ");
    if(K) printf("%c",'K');
    if(Q) printf("%c",'Q');
    if(k) printf("%c",'k');
    if(q) printf("%c",'q');
    printf(" %d %d",pos->fiftyMove, pos->ply);
}

void Parse_Fen(char* fen, S_BOARD *pos) {

    ASSERT(fen!=NULL);
    ASSERT(pos!=NULL);

    printf("inputted fen: %s\n",fen);
    //TODO proper reset
    ResetBoard(pos);

    int i = 0;
    int j = 0;

    while(fen[i]) {
        if(j < 64) {
            if(ISNUM(fen[i])){
                int num = CTONUM(fen[i]);
                while(num > 0){
                    pos->pieces64[(63-7+(j%8)-(j/8)*8)] = EMPTY;
                    pos->pieces64[63-7+(j%8)-(j/8)*8] = EMPTY;
                    j++;
                    num--;
                }
            }
            else {
                switch(fen[i]){
                    case 'p': pos->pieces64[(63-7+(j%8)-(j/8)*8)] = bp; j++; break;
                    case 'r': pos->pieces64[(63-7+(j%8)-(j/8)*8)] = br; j++; break;
                    case 'n': pos->pieces64[(63-7+(j%8)-(j/8)*8)] = bn; j++; break;
                    case 'b': pos->pieces64[(63-7+(j%8)-(j/8)*8)] = bb; j++; break;
                    case 'q': pos->pieces64[(63-7+(j%8)-(j/8)*8)] = bq; j++; break;
                    case 'k': pos->pieces64[(63-7+(j%8)-(j/8)*8)] = bk; j++; break;
                    case 'P': pos->pieces64[(63-7+(j%8)-(j/8)*8)] = wp; j++; break;
                    case 'R': pos->pieces64[(63-7+(j%8)-(j/8)*8)] = wr; j++; break;
                    case 'N': pos->pieces64[(63-7+(j%8)-(j/8)*8)] = wn; j++; break;
                    case 'B': pos->pieces64[(63-7+(j%8)-(j/8)*8)] = wb; j++; break;
                    case 'Q': pos->pieces64[(63-7+(j%8)-(j/8)*8)] = wq; j++; break;
                    case 'K': pos->pieces64[(63-7+(j%8)-(j/8)*8)] = wk; j++; break;
                    case '/': break; //TODO improve
                }
            }
        }
        else if (j < 66) {
            switch(fen[i]){
                case ' ':
                    j++;
                    break;
                case 'w':
                    pos->side = WHITE;
                    break;
                case 'b':
                    pos->side = BLACK;
                    break;
            }
        }
        else if (j < 67) {
            switch(fen[i]){
                case ' ': j++; break;
                case 'k': pos->castlePerm += BKC;printf("k small\n"); break;
                case 'q': pos->castlePerm += BQC;printf("q small\n"); break;
                case 'K': pos->castlePerm += WKC;printf("K big\n"); break;
                case 'Q': pos->castlePerm += WQC;printf("Q big\n"); break;
            }
        }
        else if (j < 68) {
            if(ISLETTER(fen[i]) && ISNUM(fen[i+1])){
                pos->enPas = FR2SQ64(LETTERTOFILE(fen[i]),CTORANK(fen[i+1]));
                i++;
            }
            else if(fen[i] == ' '){
                j++;
            }
            else if(fen[i] == '-'){
                printf("No enpassant square\n");
            }
            else{
                printf("fen %s\n",fen);
                printf("Warning: enpassant parsing at %c\n", fen[i]);
            }
        }
        else if(j < 69) {
            if(ISNUM(fen[i]))
                pos->fiftyMove = CTONUM(fen[i]);
            else if(fen[i] == ' ')
                j++;
        }
        else if(j < 70) {
            if(ISNUM(fen[i]))
                pos->ply = CTONUM(fen[i]);
            else if(fen[i] == ' ')
                j++;
        }
        i++;
    }

	pos->posKey = GeneratePosKey(pos); 
    UpdatePieceLists(pos);

    // TODO set pos key
}

void PrintPieceBoard(S_BOARD *pos) {
    printf("  A B C D E F G H\n");
    for(int r = RANK_8; r >= RANK_1; r--) {
        printf("%d",r+1);
        for(int f = FILE_A; f <= FILE_H; f++) {
            printf("%2c", printpce[pos->pieces64[FR2SQ64(f, r)]]);
        }
        printf("\n");
    }
    printf("  A B C D E F G H\n");
}

void PrintBoardSQ120(S_BOARD *pos) {
    printf("\n");
    for(int r = RANK_8; r >= RANK_1; r--) {
        printf("%d",r+1);
        for(int f = FILE_A; f <= FILE_H; f++) {
            printf(" %d",FR2SQ(f, r));
        }
        printf("\n");
    }
    printf("   A  B  C  D  E  F  G  H\n");
}

void PrintBoardInfo(S_BOARD *pos) {
    printf("To move: %c\n", pos->side ? 'b' : 'w');
    printf("Castle perm: ");
    int K = pos->castlePerm & 0x1;
    int Q = pos->castlePerm >> 1 & 0x1;
    int k = pos->castlePerm >> 2 & 0x1;
    int q = pos->castlePerm >> 3 & 0x1;
    printf("%c%c%c%c",K ? 'K' : '-', Q ? 'Q' : '-', k ? 'k' : '-', q ? 'q' : '-');
    printf("\n");
    printf("En passant: ");
    printsq64(pos->enPas);
    printf("ply %d, fiftymove %d\n",pos->ply,pos->fiftyMove);
}
void PrintBoardInfoVerbose(S_BOARD *pos) {
    printf("To move: %c\n", pos->side ? 'b' : 'w');
    printf("Castle perm: ");
    int K = pos->castlePerm & 0x1;
    int Q = pos->castlePerm >> 1 & 0x1;
    int k = pos->castlePerm >> 2 & 0x1;
    int q = pos->castlePerm >> 3 & 0x1;
    printf("%c%c%c%c",K ? 'K' : '-', Q ? 'Q' : '-', k ? 'k' : '-', q ? 'q' : '-');
    printf("\n");
    printf("En passant: ");
    printsq64(pos->enPas);
    printf("ply %d, fiftymove %d\n",pos->ply,pos->fiftyMove);
    //TODO print all the piece positions
}

void ResetBoard(S_BOARD *pos) {

    int index = 0;

    for(index = 0; index < 64; ++index) {
        pos->pieces64[index] = EMPTY;
    }
    
    for(index = 0; index < 3; ++index) {
        pos->material[index] = 0;
    }

    pos->side = BOTH;
    pos->enPas = NO_SQ;
    pos->fiftyMove = 0;

    pos->ply = 0;
    pos->hisPly = 0;

    pos->castlePerm = 0;

    pos->posKey = 0ULL;

    //TODO RESET BITBOARDS
}


//TODO rewrite
//int CheckBoard(S_BOARD *pos){
//    //printf("is sadf\n");
//    
//    //printf("%d %d \n",GeneratePosKey(pos),pos->posKey);
//	//ASSERT(GeneratePosKey(pos)==pos->posKey);
//
//    int t_pceNum[13] = {0,0,0,0,0,0,0,0,0,0,0,0};
//    int t_bigNum[2] = {0,0};
//    int t_majNum[2] = {0,0};
//    int t_minNum[2] = {0,0};
//    int t_Material[2] = {0,0};
//
//    int sq64, t_piece, t_pce_num,sq120,colour,pcount;
//
//    U64 t_pawns[3] = {0ULL,0ULL,0ULL};
//
//    t_pawns[WHITE] = pos->pawns[WHITE];
//    t_pawns[BLACK] = pos->pawns[BLACK];
//    t_pawns[BOTH] = pos->pawns[BOTH];
//
//    //check piecelists
//    for(t_piece = wp;t_piece <= bk;++t_piece){
//        for(t_pce_num = 0;t_pce_num < pos->pceNum[t_piece];++t_pce_num){
//            //printf("segfaultup\n");
//            sq120 = pos->pList[t_piece][t_pce_num]; //weil geschlagene auf NO_SQ sind
//            //if(sq120 > 1000 || sq120 < -100){
//            //    printf("segfaultmidle");
//            //    printf("t_piece count %d\n",pos->pceNum[t_piece]);
//            //    printf("sq120 %d = pos->pList[%c][%d]\n",sq120,printpce[t_piece],t_pce_num);
//            //    printf("sq120 %d = pos->pList[%c][%d]\n",sq120,printpce[wp],t_pce_num-1);
//            //    printf("\n");
//            //    printf("pos->pieces[sq120] %c==%c t_piece\n",printpce[pos->pieces[sq120]],printpce[t_piece]);
//            //}
//            ASSERT(VALIDPIECE(pos->pieces[sq120])); //segfault
//            //printf("segfaultlow\n");
//            //printf("piece num %d\n", pos->pieces[sq120]);
//            //printsq(sq120);
//            //PrintPieceBoard(pos);
//            //printf("\n");
//            if(pos->pieces[sq120]!=t_piece){
//                printsq(sq120);
//                printf("pos->pieces[sq120] %c = %c t_piece\n",printpce[pos->pieces[sq120]],printpce[t_piece]);
//            }
//            ASSERT(pos->pieces[sq120]==t_piece);
//            //printf("sq120 %d = pos->pList[%c][%d]\n",sq120,printpce[wp],1);
//        }
//    }
//
//    //check piece count and other counters
//    for(sq64=0;sq64<64;++sq64){
//        sq120 = SQ120(sq64);
//        t_piece = pos->pieces[sq120];
//        t_pceNum[t_piece]++;
//        colour = colList[t_piece];
//        if( isBig[t_piece] == True) t_bigNum[colour]++; 
//        if( isMin[t_piece] == True) t_minNum[colour]++; 
//        if( isMaj[t_piece] == True) t_majNum[colour]++; 
//
//        t_Material[colour] += valList[t_piece];
//    }
//
//    for(t_piece = wp;t_piece <= bk;++t_piece){
//        ASSERT(t_pceNum[t_piece] == pos->pceNum[t_piece]);
//    }
//    //check bitboard
//    //PrintPieceBoard(pos);
//    //printf("both %d white %d/%d black %d/%d\n",CNT(pos->pawns[BOTH]),CNT(pos->pawns[WHITE]), pos->pceNum[wp],CNT(pos->pawns[BLACK]), pos->pceNum[bp]);
//    ASSERT((CNT(pos->pawns[WHITE])+CNT(pos->pawns[BLACK])) == CNT(pos->pawns[BOTH])); // TODO fails
//    pcount = CNT(t_pawns[WHITE]);
//    ASSERT(pcount == pos->pceNum[wp]);
//    pcount = CNT(t_pawns[BLACK]);
//    ASSERT(pcount == pos->pceNum[bp]);
//    pcount = CNT(t_pawns[BOTH]);
//    ASSERT(pcount == (pos->pceNum[wp] + pos->pceNum[bp]));
//
//    // check bitboards squares
//    while(t_pawns[WHITE]){
//        sq64 = POP(&t_pawns[WHITE]);
//        ASSERT(pos->pieces[SQ120(sq64)] == wp);
//    }
//    while(t_pawns[BLACK]){
//        sq64 = POP(&t_pawns[BLACK]);
//        ASSERT(pos->pieces[SQ120(sq64)] == bp);
//    }
//    while(t_pawns[WHITE]){
//        sq64 = POP(&t_pawns[BOTH]);
//        ASSERT((pos->pieces[SQ120(sq64)] == wp) || (pos->pieces[SQ120(sq64)] == bp) );
//    }
//
//    return 1;
//    //PrintPieceBoard(pos);
//    //PrintBoardInfo(pos);
//    //printf("Mat t white %d mat white %d mat t black %d mat black %d",t_Material[WHITE],pos->material[WHITE],t_Material[BLACK],pos->material[BLACK]);
//
//    //semi wichtig
//    
//    //ASSERT(t_Material[WHITE] == pos->material[WHITE] && t_Material[BLACK] == pos->material[BLACK]); //TODO fails (is ok in make move)
//    //ASSERT(t_minNum[WHITE] == pos->minNum[WHITE] && t_minNum[BLACK] == pos->minNum[BLACK]);
//    //ASSERT(t_majNum[WHITE] == pos->majNum[WHITE] && t_majNum[BLACK] == pos->majNum[BLACK]);
//    //ASSERT(t_bigNum[WHITE] == pos->bigNum[WHITE] && t_bigNum[BLACK] == pos->bigNum[BLACK]);
//
//    ASSERT(pos->side == WHITE || pos->side==BLACK);
//    //TODO poskey
//    ASSERT(pos->enPas == NO_SQ || (RANKSQ(pos->enPas) == RANK_6 && pos->side == WHITE) || (RANKSQ(pos->enPas) == RANK_3 && pos->side == BLACK));
//    // check if pawnbitboard is ok
//    //white pawns
//    U64 w = pos->pawns[WHITE];
//    int i;
//    for(i = 0;i < 64;i++){
//        if((w >> i) & 0x1){
//            // there must be a white pawn on the board
//            ASSERT(pos->pieces[SQ120(i)] == wp);
//        }
//    }
//    U64 b = pos->pawns[BLACK];
//    for(i = 0;i < 64;i++){
//        if((b >> i) & 0x1){
//            // there must be a white pawn on the board
//            ASSERT(pos->pieces[SQ120(i)] == bp);
//        }
//    }
//    return True;
//}

