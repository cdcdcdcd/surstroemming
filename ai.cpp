#include "defs.hpp"
#include "stdio.h"
#include <time.h>

void RandomMove(S_BOARD *pos){ //TODO assumes there is a legal move
    S_MOVELIST list;  //init an empty list
    list.count = 0;
    GenerateAllMoves(&list,pos);

    int len = list.count; //maybe -1
    if(len == 0){ 
        printf("no legal moves\n");
        scanf("");
    }
    S_MOVE move = list.moves[rand()%len];
    if(!MakeMove(pos,move.move)){
        UndoMove(pos);
        RandomMove(pos);
    }
}

static void CheckUp(S_SEARCHINFO *info) {
	// check if time up, or interrupt from GUI
	if( (GetTimeMs() - info->start) > info->stop) {
		info->stopped = True;
	}
		
	ReadInput(info);
}


static void clearforsearch(S_BOARD* pos, S_SEARCHINFO* info){

    for (int i = 0; i < 13; ++i) {
        for (int j = 0; j < BRD_SQ_NUM; j++) { //TODO 64
            pos->searchHist[i][j] = 0; 
        } 
    }

    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < MAXDEPTH; j++) {
            pos->searchKill[i][j] = 0; 
        } 
    }
    
    //TODO clear pv table
    
	pos->HashTable->overWrite=0;
	pos->HashTable->hit=0;
	pos->HashTable->cut=0;
   

    pos->ply = 0; 

    info->start = GetTimeMs(); //TODO
    info->stopped = 0;
    info->nodes = 0;

	info->fh = 0;
	info->fhf = 0;

}

int CheckCheckMate(S_MOVELIST *list, S_BOARD *pos){ //TODO fix
    int len = list->count;
    int i = 0;
    S_MOVE move; 
    while(i<len){
        move = list->moves[i];
        if(!MakeMove(pos,move.move)){
            i++;
            UndoMove(pos);
        }
        else
        {
            return 0;
        }
    }
    return 1;
}


static void PickNextMove(int moveNum, S_MOVELIST *list) {

	S_MOVE temp;
	int index = 0;
	int bestScore = 0; 
	int bestNum = moveNum;
	
	for (index = moveNum; index < list->count; ++index) {
		if (list->moves[index].score > bestScore) {
			bestScore = list->moves[index].score;
			bestNum = index;
		}
	}
	temp = list->moves[moveNum];
	list->moves[moveNum] = list->moves[bestNum];
	list->moves[bestNum] = temp;
}


static int Quiescence(int alpha, int beta, S_BOARD *pos) {
    int side = pos->side;
	int OldAlpha = alpha;
	//int BestMove = NOMOVE;
	
    if(IsRepetition(pos) || pos->fiftyMove >= 100) return 0; 

	if(pos->ply > MAXDEPTH - 1) {
		return EvalPosition(pos);
	}

	int value = EvalPosition(pos);

	if(value >= beta) {
		return beta;
	}
	
	if(value > alpha) {
		alpha = value;
	}

	if(pos->ply > MAXDEPTH - 1) {
		return EvalPosition(pos);
	}
	
    S_MOVELIST list; 
    list.count = 0;
    GenerateCaps(&list,pos);
      
	value = -INF;
	
    int MoveNum = 0;
    for(MoveNum = 0;MoveNum < list.count;++MoveNum){

        PickNextMove(MoveNum,&list);
        S_MOVE move = list.moves[MoveNum];

        if( !MakeMove(pos,move.move)){
            UndoMove(pos); 
            continue;
        }

		value = -Quiescence(-beta, -alpha, pos);		
        UndoMove(pos);


        if(value >= beta)
            return beta;
        if(value > alpha){
            alpha = value;
			//BestMove = list.moves[MoveNum].move;
        }
    }
    return alpha;
}


int AlphaBeta(int depth, int alpha, int beta,S_BOARD *pos,int nullmove,S_SEARCHINFO* info){

	if(( info->nodes & 2047 ) == 0) {
		CheckUp(info);
	}

    info->nodes++;

    //if(MaterialDraw(pos)) return 0;
    if(IsRepetition(pos) || pos->fiftyMove >= 100) return 0; 

    if(!depth) {
        //return EvalPosition(pos);
        return Quiescence(alpha,beta,pos); 
    }
    

	if(pos->ply > MAXDEPTH - 1) {
		return EvalPosition(pos);
	}

    int side = pos->side;
    int legal = 0;


    int k = side ? bk : wk;
    int incheck = BitSqAttacked(lsb(pos->piecebb[k]),side,pos);

	int OldAlpha = alpha;
	int BestMove = NOMOVE;

    int value = -INF;

	int PvMove = NOMOVE;
    
    /*null move makes it harder to find mate */
    if(incheck){
        depth++;
    }

    int hashval;
    if (ProbeHashEntry(pos,&PvMove, &hashval, alpha, beta, depth) == True){ 
        pos->HashTable->cut++; 
        return hashval;
    }

    if(!incheck&&nullmove && !incheck && pos->ply && depth >= 3 && HasNonPawn(pos) > (depth > 8)) { //check how many 
        MakeNullMove(pos);
        value = -AlphaBeta(depth-2,-beta,-beta+1,pos,False,info);
        UndoNullMove(pos);

        if(value >= beta){
            return beta;
        }
    } 

    S_MOVELIST list;  //init an empty list
    list.count = 0;
    GenerateAllMoves(&list,pos);

    int Kill1 = pos->searchKill[0][pos->ply];
    int Kill2 = pos->searchKill[1][pos->ply];
    int MoveNum = 0;

	if( PvMove != NOMOVE) {
		for(MoveNum = 0; MoveNum < list.count; ++MoveNum) {
			if( list.moves[MoveNum].move == PvMove) {
				list.moves[MoveNum].score = INF;
				break;
			}
		}
	}

    int foundPv = False;

    for(MoveNum = 0;MoveNum < list.count;++MoveNum){
        PickNextMove(MoveNum,&list);
        S_MOVE move = list.moves[MoveNum];
        if( !MakeMove(pos,move.move)){
            UndoMove(pos); 
            continue;
        }
        legal++;


        if(foundPv){
            value = -AlphaBeta(depth-1,-alpha-1,-alpha,pos,nullmove,info);
            if(value > alpha && value < beta){
                value = -AlphaBeta(depth-1,-beta,-alpha,pos,nullmove,info);
            }
        }
        else{
            value = -AlphaBeta(depth-1,-beta,-alpha,pos,nullmove,info);
        }

        UndoMove(pos);

		if(info->stopped == True) {
			return 0;
		}

        if(value >= beta){
            if(legal == 1){
                info->fhf++;
            }
            info->fh++;

			if(!(list.moves[MoveNum].move & FLAGCAP)) {
		        pos->searchKill[1][pos->ply] = pos->searchKill[0][pos->ply];
		        pos->searchKill[0][pos->ply] = list.moves[MoveNum].move;
			}

            StoreHashEntry(pos, BestMove, value, HFBETA, depth); 
            return beta;
        }
        if(value > alpha){
            foundPv = True;
            alpha = value;
			BestMove = list.moves[MoveNum].move;
			if(!(list.moves[MoveNum].move & FLAGCAP)) {
                pos->searchHist[pos->pieces64[FROMSQ(BestMove)]][TOSQ(BestMove)] += depth*depth;
			}

        }
    }

    if(!legal){
        if(incheck){
            return -MATE + pos->ply; 
        }else{
            return 0;
        }
    }

    //not in the loop 
	if(alpha != OldAlpha) {
		//StorePvMove(pos, BestMove);
        StoreHashEntry(pos, BestMove, value, HFEXACT, depth); // score = bestscore??
	}else{
        StoreHashEntry(pos, BestMove, alpha, HFALPHA, depth); 
    }

    return alpha;
}

//TODO better name
int iterdeep(S_BOARD* pos, S_SEARCHINFO* info){
    printf("depth %d\n", info->depth);
    
    int move = GetBookMoveBinary(pos);
    //int move = NOMOVE;
    //if bookmove then return bookmove
    if(move != NOMOVE){
        //TODO IF UCI MODE
		printf("info score cp %d depth %d nodes %ld time %d \n", 0, 0,0,0);
	    printf("bestmove %s\n",PrMove(move));
        MakeMove(pos,move);
        PrintPieceBoard(pos);
        PrintBoardInfo(pos);
        return move;
    }
    else{
        printf("failed to find opneing move\n");
    }

    int score = - INF;
    int pvMoves = 0;
    int pvNum = 0;

    int alpha;
    int beta;
    int delta;

    int searchDepth;

    int WINDOW = 16;

    int cfh = 0;
    clearforsearch(pos,info);

    int mat = pos->material[WHITE]+pos->material[BLACK];

    if(mat <= 2*ENDGAMEMATERIAL){
        info->depth++;
        if(mat <= 2*ENDGAMEMATERIAL2){
            info->depth++;
            if(mat <= 2*ENDGAMEMATERIAL3){
                info->depth++;
            }
        }
    }

    for (int i = 1; i <= info->depth; ++i) {

        alpha = -INF;
        beta = INF;
        delta = INF;

        searchDepth = i;

        if (i >= 5) {
          alpha = MAX(score - WINDOW, -INF);
          beta = MIN(score + WINDOW, INF);
          delta = WINDOW;
        }

        while(!info->stopped){
            //printf("search depth %d,%d with delta window %d, alpha %d, beta %d\n",i,searchDepth,delta,alpha,beta);
            score = AlphaBeta(searchDepth,alpha,beta,pos,True,info);

            printf("ordering %.2f\n",(info->fhf/info->fh));
            
            if (score <= alpha) {
                beta = (alpha + 3*beta) / 4;
                alpha = MAX(alpha - delta, -INF);
            }
            else if (score >= beta) {
                beta = MIN(beta + delta, INF);
                if (score < ISMATE && score > -ISMATE) {
                    searchDepth -= 1;
                    searchDepth = MAX(1, searchDepth);
                }
            } else {
                move = GetMovePvTable(pos); 
                break;
            }

            delta += 2*delta / 3;
        }
        if(score > ISMATE || score < -ISMATE){
            move = GetMovePvTable(pos); 
            break;
        }

        //if((GetTimeMs()-(info->start) < info->mintime && info->depth == i)){
        //    info->depth++;
        //}
        //TODO IF UCI MODE
	    printf("info score cp %d depth %d nodes %ld time %d \n",
	    			score, i,info->nodes,GetTimeMs()-info->start);
        if(info->stopped){
            break;
        }
    }


    //move = GetMovePvTable(pos);  //STILL BUGGY

    //printf("time in ms %d\n",(GetTimeMs()-(info->start))); 
    //printf("nodes %ld\n",info->nodes); 
    //printf("nodes per second %f\n",(float)info->nodes/(GetTimeMs()-(info->start))*1000); 
    //printf("time in seconds %f\n",(float)(GetTimeMs()-(info->start))/1000);

    //TODO IF UCI MODE
	printf("bestmove %s\n",PrMove(move));
    MakeMove(pos,move);
    return move;
}

