#include "defs.hpp"
#include "stdio.h"

int InputToMove(S_BOARD* pos,int from, int to, int prom){
    S_MOVELIST list; 
    list.count = 0;
    GenerateAllMoves(&list,pos);

    for(int i = 0;i < list.count;i++){
        int mv = list.moves[i].move;
        if(from == FROMSQ(mv) && to == TOSQ(mv) && (prom == PROMOTED(mv) || 0 == PROMOTED(mv))){
            S_MOVE move = list.moves[i];
            if(!MakeMove(pos,move.move)){
                printf("illegal move\n");
                UndoMove(pos);
                return NOMOVE;
            }
            UndoMove(pos);
            return move.move;
        }
    }
    printf("invalid move\n");
    return NOMOVE;
}

int MakeInput(S_BOARD* pos, int from, int to, int prom){
    int move = InputToMove(pos,from,to, prom);
    if(move != NOMOVE) {
        MakeMove(pos,move);
        return 1;
    }
    return 0;
}

int InputMove(S_BOARD *pos,int frominput, int toinput){
    int piece = pos->pieces64[frominput];
    int isProm = (RANKSQ64(toinput) == RANK_1 && piece == bp) || (RANKSQ64(toinput) == RANK_8 && piece == wp);
    int prom = 0;
    if(isProm) {
        //TODO ask for prom piece
        prom = pos->side ? bq : wq;
    }
    return MakeInput(pos, frominput, toinput, prom);
}


//vice
int ParseMove(char *ptrChar, S_BOARD *pos) {

	if(ptrChar[1] > '8' || ptrChar[1] < '1') return NOMOVE;
    if(ptrChar[3] > '8' || ptrChar[3] < '1') return NOMOVE;
    if(ptrChar[0] > 'h' || ptrChar[0] < 'a') return NOMOVE;
    if(ptrChar[2] > 'h' || ptrChar[2] < 'a') return NOMOVE;

    int from = FR2SQ64(ptrChar[0] - 'a', ptrChar[1] - '1');
    int to = FR2SQ64(ptrChar[2] - 'a', ptrChar[3] - '1');

    printf("parse mvoe\n");
    printsq64(from);
    printsq64(to);

	S_MOVELIST list[1];
    GenerateAllMoves(list,pos);
    printf("found %d moves\n",list->count);
    int MoveNum = 0;
	int Move = 0;
	int PromPce = EMPTY;

	for(MoveNum = 0; MoveNum < list->count; ++MoveNum) {
		Move = list->moves[MoveNum].move;
        int f = FROMSQ(Move);
        int t = TOSQ(Move);
        printsq64(f);
        printsq64(t);
		if(FROMSQ(Move)==from && TOSQ(Move)==to) {
			PromPce = PROMOTED(Move);
			if(PromPce!=EMPTY) {
				if((PromPce == wr || PromPce == br)&& ptrChar[4]=='r') {
					return Move;
				} else if((PromPce == wb || PromPce == bb)  && ptrChar[4]=='b') {
					return Move;
				} else if((PromPce == wq || PromPce == bq)  && ptrChar[4]=='q') {
					return Move;
				} else if((PromPce == wn || PromPce == bn) && ptrChar[4]=='n') {
					return Move;
				}
				continue;
			}
			return Move;
		}
    }

    printf("no move found........\n");
    return NOMOVE;
}


char *PrMove(const int move) {

	static char MvStr[6];

	int ff = FILESQ64(FROMSQ(move));
	int rf = RANKSQ64(FROMSQ(move));
	int ft = FILESQ64(TOSQ(move));
	int rt = RANKSQ64(TOSQ(move));

	int promoted = PROMOTED(move);

	if(promoted) {
		char pchar = 'q';
		if(promoted == wn || promoted == bn) {
			pchar = 'n';
		} else if(promoted == wr || promoted == br)  {
			pchar = 'r';
		} else if(promoted == wb || promoted == bb)  {
			pchar = 'b';
		}
		sprintf(MvStr, "%c%c%c%c%c", ('a'+ff), ('1'+rf), ('a'+ft), ('1'+rt), pchar);
	} else {
		sprintf(MvStr, "%c%c%c%c", ('a'+ff), ('1'+rf), ('a'+ft), ('1'+rt));
	}

	return MvStr;
}

