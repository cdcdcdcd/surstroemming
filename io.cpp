#include "stdio.h"
#include "defs.hpp"

void printsq64(const int sq64) {
    printf("%c%c\n", (sq64 % 8)+'A',(sq64/8)+'1');
}

char* sprintsq64(const int sq64) {
    static char sqstr[3];
    sprintf(sqstr,"%c%c", FILESQ64(sq64)+'A',RANKSQ64(sq64)+'1');
    return sqstr;
}

void printmv(int mv) {
    printf("from: %s ",sprintsq64(FROMSQ((mv))));
    printf("to: %s ",sprintsq64(TOSQ((mv))));
    printf("cptrd: %c ",printpce[CAPTURED((mv))]);
    printf("prmtd: %c ",printpce[PROMOTED((mv))]);
    printf(" %c%c%c",CASTLE(mv) ? 'c' : '-',ENPAS(mv) ? 'e' : '-', PAWNSTART(mv) ? 'p' : '-');
    return;
}

void printmvl(int mv) {
    printf("%s->",sprintsq64(FROMSQ((mv))));
    printf("%s",sprintsq64(TOSQ((mv))));
    printf(" %c ",printpce[CAPTURED((mv))]);
    printf(" %c ",printpce[PROMOTED((mv))]);
    printf(" %c%c%c",CASTLE(mv) ? 'c' : '-',ENPAS(mv) ? 'e' : '-', PAWNSTART(mv) ? 'p' : '-');
    return;
}
void printmvsl(int mv) {
    printf("%s->",sprintsq64(FROMSQ((mv))));
    printf("%s ",sprintsq64(TOSQ((mv))));
    return;
}

void printmvlist(S_MOVELIST* list){
    for(int i = 0;i < list->count;i++){
        //printmvsl(list->moves[i].move); 
        printmvl(list->moves[i].move); 
        printf("\n");
    }
}

void printscore(int score) { //maybe add +- depending on whos turn it is
    if(score > MATE - 500){
        printf("M%d\n",MATE-score);
    }
    else{
        printf("%d\n",score);
    }
}

void printattacked(S_BOARD* pos){
    printf("\n");
    for (int i = 0; i < 8; ++i) for(int j = 0; j < 8; j++){
        int sq = (j+8*(7-i));
        if(BitSqAttacked(sq,pos->side,pos)) printf("X ");
        else printf("- ");
        if(j == 7) printf("\n");
    }
}
