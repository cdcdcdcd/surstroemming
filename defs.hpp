#ifndef DEFS_H
#define DEFS_H init.cpp

#include "stdlib.h"


//#define DEBUG

#ifndef DEBUG
#define ASSERT(n)
#else
#define ASSERT(n) \
if(!(n)) { \
printf("%s - Failed", #n); \
printf("On %s ", __DATE__); \
printf("AT %s ", __TIME__); \
printf("In File %s ", __FILE__); \
printf("At Line %d\n", __LINE__); \
exit(1);}
#endif


typedef unsigned long long U64;

#define BRD_SQ_NUM 120

#define MAXGAMEMOVES 2048
#define MAXPOSMOVES 256
#define MATE 100000
#define INF 200000
#define GIVEUP 3
#define CHECKMATED 2
#define DRAW 1
#define ENDGAMEMATERIAL ( valList[wk] + 2*valList[wp] + 2*valList[wn] ) 
#define ENDGAMEMATERIAL2 ( valList[wk] + 1*valList[wp] + 1*valList[wn] ) 
#define ENDGAMEMATERIAL3 ( valList[wk] + 2*valList[wp] ) 
#define MAXDEPTH 64
#define NOMOVE 0
#define ISMATE (MATE - MAXDEPTH)

#define SIZE 800
#define FPS 30
//bit
#define NE 9
#define N 8
#define NW 7
#define E 1
#define W -1
#define SE -7
#define S -8
#define SW -9

#define NEi 0
#define Ni 1
#define NWi 2
#define Ei 3
#define Wi 4
#define SEi 5
#define Si 6
#define SWi 7

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

#define lsb(bb) (__builtin_ctzll(bb))
#define msb(bb) (63 ^ __builtin_clzll(bb))

#define popLsb(bb) ((bb) &= (bb)-1)

//#define ShiftE(bb) (((bb) & ~H_FILE) << 1)
//#define ShiftNE(bb) (((bb) & ~H_FILE) >> 7)
//#define ShiftSW(bb) (((bb) & ~A_FILE) << 7)
//#define ShiftNW(bb) (((bb) & ~A_FILE) >> 9)

#define START_FEN "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"

enum { EMPTY, wp, wn, wb, wr, wq, wk, bp, bn, bb, br, bq, bk };
enum { FILE_A, FILE_B, FILE_C, FILE_D, FILE_E, FILE_F, FILE_G, FILE_H, FILE_NONE};
enum { RANK_1, RANK_2, RANK_3, RANK_4, RANK_5, RANK_6, RANK_7, RANK_8, RANK_NONE};
enum { WHITE, BLACK, BOTH};


enum {//TODO delete
    A1 = 21, B1, C1, D1, E1, F1, G1, H1,
    A2 = 31, B2, C2, D2, E2, F2, G2, H2,
    A3 = 41, B3, C3, D3, E3, F3, G3, H3,
    A4 = 51, B4, C4, D4, E4, F4, G4, H4,
    A5 = 61, B5, C5, D5, E5, F5, G5, H5,
    A6 = 71, B6, C6, D6, E6, F6, G6, H6,
    A7 = 81, B7, C7, D7, E7, F7, G7, H7,
    A8 = 91, B8, C8, D8, E8, F8, G8, H8, NO_SQ
};

//#define NO_SQ 99

//TODO remove
enum {
    a1 , b1, c1, d1, e1, f1, g1, h1,
    a2 , b2, c2, d2, e2, f2, g2, h2,
    a3 , b3, c3, d3, e3, f3, g3, h3,
    a4 , b4, c4, d4, e4, f4, g4, h4,
    a5 , b5, c5, d5, e5, f5, g5, h5,
    a6 , b6, c6, d6, e6, f6, g6, h6,
    a7 , b7, c7, d7, e7, f7, g7, h7,
    a8 , b8, c8, d8, e8, f8, g8, h8
};

enum { False, True };

enum { WKC = 1, WQC = 2, BKC =4, BQC =8 };

typedef struct af {
    U64 posKey;
    int move;
} S_PVENTRY;

typedef struct vja {
    S_PVENTRY *pTable;
    int numEntries;
} S_PVTABLE;

enum {  HFNONE, HFALPHA, HFBETA, HFEXACT};

typedef struct {
	U64 posKey;
	int move;
	int score;
	int depth;
	int flags;
} S_HASHENTRY;

typedef struct {
	S_HASHENTRY *pTable;
	int numEntries;
	int newWrite;
	int overWrite;
	int hit;
	int cut;
} S_HASHTABLE;

// search info
// depth
// score
// nodes
// best move
// dunno
// start
// stop idk
// DEPTH
// maxtime

typedef struct searchinfo {
    int depth;
    int score;
    int infinit; //infinitesearch bool
    long nodes; //nodes searched
    int start; //starttime
    int stop; //stoptime

    int quit; //??

    int timeset;

    int mintime;

    int stopped;

    float fh; //fail high
    float fhf; //fail high first
} S_SEARCHINFO;


typedef struct  {
    int plays_white;
    int plays_black;
    S_SEARCHINFO info_white;
    S_SEARCHINFO info_black;
} S_PLAYER_PC;


typedef struct mov {
    int move;
    int score;
} S_MOVE;

typedef struct movlist {
    S_MOVE moves[MAXPOSMOVES];
    int count;
} S_MOVELIST;

typedef struct hist {
    int move;
    int fiftyMove;
    int enPas;
    int castlePerm;
    U64 posKey;
} S_HISTORY;


typedef struct BOARD {

    // piece on square (board)
    //int pieces[BRD_SQ_NUM]; //TODO make this index 64
    int pieces64[64];
    U64 allpawns;
    U64 piecebb[13]; //TODO merge wih pawns??

    U64 occu[3];  
    //U64 pinned;   
    //U64 checkers;      // squares of pieces giving check 
    
    int side;
    int enPas;

    int ply;
    int hisPly;
    int fiftyMove;

    S_HISTORY history[MAXGAMEMOVES];

    int castlePerm;

    U64 posKey;

    /// number of.. each type of piece
    //int pceNum[13];
    //int pList[13][10];
    /// number of.. big maj min pieces

    //int bigNum[2];
    //int majNum[2];
    //int minNum[2]; //TODO probably unnecessary

    int material[2];

    int searchHist[13][BRD_SQ_NUM]; //??
    int searchKill[2][MAXDEPTH];

    //S_PVTABLE PvTable[1]; // TODO is this still used???
    S_HASHTABLE HashTable[1];
	int PvArray[MAXDEPTH]; // TODO clean this up

} S_BOARD;


/* MOve how
 * 0000 0000 0000 0000 0000 0111 1111 -> From 2^7=128
 * 0000 0000 0000 0011 1111 1000 0000 -> To 2^7 = 128
 * 0000 0000 0011 1100 0000 0000 0000 -> Captured what piece
 * 0000 0000 0100 0000 0000 0000 0000 -> En passant
 * 0000 0000 1000 0000 0000 0000 0000 -> Pawnstart
 * 0000 1111 0000 0000 0000 0000 0000 -> Promoted Piece
 * 0001 0000 0000 0000 0000 0000 0000 -> Castle
 */
#define FLAGENPAS 0x40000
#define FLAGPWNSSTRT 0x80000
#define FLAGCSTL 0x1000000
#define FLAGCAP 0x7c000

#define FROMSQ(m) ( (m) & 0x7F )
#define TOSQ(m) ( (m)>>7 & 0x7F )
#define CAPTURED(m) ( (m)>>14 & 0x0F )
#define PROMOTED(m) ( (m)>>20 & 0x0F )
//
#define ENPAS(m) ( (m)  & FLAGENPAS )
#define PAWNSTART(m) ( (m)  & FLAGPWNSSTRT )
#define CASTLE(m) ( (m)  & FLAGCSTL )
//#define ENPAS(m) ( (m) >>18 & 0x1 )
//#define PAWNSTART(m) ( (m) >>19 & 0x1 )
//#define CASTLE(m) ( (m) >>24 & 0x1 )

//#define FLAGPROM 0xF00000

#define MV(from,to,cap,ep,pawnstart,prom,castle) ( (from) | ((to) << 7) | ((cap) << 14) | (ep) << 18 | (pawnstart) << 19 | ((prom) << 20) | ((castle) << 24))
#define MVF(from,to,cap,prom,flags) ( (from) | ((to) << 7) | ((cap) << 14) | ((prom) << 20) | flags)


/* macros */
#define FR2SQ(f,r) ( (21 + (f) ) + ( (r) * 10 ) ) //TODO remove
#define FR2SQ64(f,r) ( ( (f) ) + ( (r) * 8 ) )

#define SQ64(sq120) Sq120ToSq64[(sq120)]
#define SQ120(sq64) (Sq64ToSq120[(sq64)])

#define ISNUM(c) ((c-'0' >= 0 && c-'9' <= 0))
#define CTONUM(c) ((c-'0'))
#define CTORANK(c) ((c-'0'-1))
#define ISLETTER(c) ((c-'A' >= 0 && c-'z' <= 0))
#define LETTERTONUM(c) ((c-'a'))
#define LETTERTOFILE(c) ((c-'a'))
#define CLRBIT(bb,sq) ((bb) &= ClearMask[(sq)])
#define SETBIT(bb,sq) ((bb) |= SetMask[(sq)])
#define ISOFFBOARD(sq120) (((sq120) < 21 || (sq120) > 98 || ((sq120) % 10 == 0) || ((sq120) % 10 == 9)))
#define FILESQ(sq120) ((sq120)%10-1)
#define RANKSQ(sq120) ((sq120)/10-2)
#define FILESQ64(sq64) ((sq64)%8)
#define RANKSQ64(sq64) ((sq64)/8)
#define VALIDPIECE(piece) ((piece >= 0 && piece <= 12))

/* globals */
extern int Sq120ToSq64[BRD_SQ_NUM];//TODO delete
extern int Sq64ToSq120[64]; //TODO delete


extern void PrintBitBoard(U64 bb);
extern void ResetBoard(S_BOARD *pos);
extern void Parse_Fen(char *fen, S_BOARD *pos);
extern void PrintPieceBoard(S_BOARD *pos);
extern void PrintBoardInfo(S_BOARD *pos);
extern int CNT(U64 bb);
extern int POP(U64 *bb);
extern U64 SetMask[64];
extern U64 ClearMask[64];
extern int SqAttacked(const int sq, const int side, const S_BOARD *pos);
extern int SqAttacked64(const int sq, const int side, const S_BOARD *pos);
extern const char printpce[];
extern void printsq(int sq120);
extern void printsq64(const int sq64);
extern char* sprintsq(const int sq120);
extern void printmv(const int mv);
extern void printmvl(int mv);
extern void PrintBoardSQ120(S_BOARD *pos);

/*init*/
extern U64 PieceKeys[13][64];
extern U64 SideKey;
extern U64 CastleKeys[16];
extern U64 PassedMask[64][2];
extern U64 IsolatedMask[64];
extern U64 RankMask[64];
extern U64 rayAttacks[8][64]; 
extern U64 KnightAttacks[64];
extern U64 KingAttacks[64];
extern U64 PawnAttacks[2][64];
extern U64 PawnQuiet[2][64];
extern U64 PawnBlock[2][64];
extern U64 WKCbb;
extern U64 WQCbb;
extern U64 BKCbb;
extern U64 BQCbb;
extern const int dirs[8];

/*moves*/
extern void GenAllMoves( const S_BOARD *pos, S_MOVELIST *list);
extern void GenAllCaps( const S_BOARD *pos, S_MOVELIST *list);
extern int MoveExists(S_BOARD *pos, const int move);
extern int BitSqAttacked(const int sq, const int side, const S_BOARD *pos);
extern void GenerateCaps(S_MOVELIST* list, S_BOARD* pos);

extern const int NDir[8]; //TODO is this used?
extern const int RDir[4];
extern const int BDir[4];
extern const int KDir[8];
extern const int PDir[][3];

/*bitmoves*/
extern void GeneratePieceMoves(S_MOVELIST* list, U64 movers, S_BOARD* pos, int piece);
extern void GeneratePawnMoves(S_MOVELIST* list, U64 movers, S_BOARD* pos);
extern void GenerateAllMoves(S_MOVELIST* list, S_BOARD* pos);

/*hashkeys*/
extern U64 GeneratePosKey(const S_BOARD *pos);

//maps piecenumber to eigenschaft
extern int isBig[13];
extern int isMaj[13];
extern int isMin[13];
extern int valList[13]; 
extern int colList[13];
extern int prompce[2][4];

/* functions */
extern void AllInit();
extern int MakeMove(S_BOARD *pos,int mv);
extern void UndoMove(S_BOARD *pos);
extern void HumanMove(S_MOVELIST *list,S_BOARD *pos);
extern void printfen(S_BOARD *pos);
extern int CheckBoard(S_BOARD *pos);
extern int CheckBoardNew(S_BOARD* pos);
extern void PerftTest(int depth, S_BOARD *pos);
extern void UndoNullMove(S_BOARD* pos);
extern void MakeNullMove(S_BOARD* pos);
extern void RandomTest(int times, S_BOARD* pos);

/*ai*/
extern void RandomMove(S_BOARD *pos);
extern void FirstMove(S_MOVELIST *list,S_BOARD *pos);
extern void TakeMove(S_MOVELIST *list,S_BOARD *pos);
extern int CheckCheckMate(S_MOVELIST *list,S_BOARD *pos);
extern int AlphaBeta(int depth, int alpha, int beta,S_BOARD *pos,int nullmove,S_SEARCHINFO* info);
extern int Search(int DEPTH, int alpha, int beta,S_BOARD *pos, S_MOVE *bestmove,S_SEARCHINFO* info);
extern int iterdeep(S_BOARD* pos, S_SEARCHINFO* info);

/*io*/
extern void printscore(int score);
extern void printmvsl(int mv);
extern void printmvlist(S_MOVELIST* list);
extern void printattacked(S_BOARD* pos);

/*eval*/
extern int EvalPosition(const S_BOARD *pos);
extern int MaterialDraw(const S_BOARD *pos);
extern int CheckIfOver(S_BOARD* pos);
extern int IsRepetition(S_BOARD* pos);

/*moves*/
extern void InitMvvLva();

/*input*/
extern int InputMove(S_BOARD *pos,int frominput, int toinput);
extern int MakeInput(S_BOARD* pos, int from, int to, int prom);
extern int InputToMove(S_BOARD* pos,int from, int to, int prom);
extern int ParseMove(char *ptrChar, S_BOARD *pos);
extern char *PrMove(const int move);

/*misc*/
int GetTimeMs();
extern void ReadInput(S_SEARCHINFO *info);

/*pvtable*/
extern void InitPvTable(S_PVTABLE *table);
extern void StorePvMove(const S_BOARD *pos, const int move);
extern int GetMovePvTable(const S_BOARD *pos);
extern int GetPvLine(const int depth, S_BOARD *pos);
extern void ClearHashTable(S_HASHTABLE *table);
extern void InitHashTable(S_HASHTABLE *table, const int MB);
extern int ProbeHashEntry(S_BOARD *pos, int *move, int *score, int alpha, int beta, int depth);
extern void StoreHashEntry(S_BOARD *pos, const int move, int score, const int flags, const int depth);

/*bit*/
extern int popAndGetLsb(U64* bb);
extern int HasNonPawn(S_BOARD* pos);

/*polyglot*/
extern void InitPolyBook();
extern int GetBookMove(S_BOARD *pos);
extern void PrintMovesOrdered();
extern int GetBookMoveBinary(S_BOARD* pos);

/*gui*/
//void initimg(S_IMAGES* images,SDL_Renderer* renderer);
//void draw(SDL_Renderer* renderer,S_IMAGES* images, S_BOARD* pos,SDL_Rect* rect,int from, int to);
extern void gui(S_BOARD* pos,S_MOVELIST* list, S_PLAYER_PC* player_pc);

/*uci*/
extern void Uci_Loop(S_BOARD *pos, S_SEARCHINFO *info);

#endif

