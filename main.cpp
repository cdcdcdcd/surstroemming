#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "defs.hpp"
#include <unistd.h>

// notes
// TODO improve parsefen
// TODO king position endgame and opening distinction
// TODO non blocking search for move
// TODO ai and input maybe better pass by reference
// TODO migrate to pure C
// TODO clean up defs
// TODO pinned pieces
// TODO move repetition irl
// TODO BUG print principal variation
// TODO logo (lul)
// TODO custom chess pieces
// TODO improve eval
// TODO increase ply when few pieces??
// TODO clean up code
// TODO ponder
// TODO typedef unsigned char bool
// TODO inline functions

// TODO get rid of occu both??? because piece[empty] == ~occu[both]
// TODO improve bitsetting
// TODO recheck move ordering
// TODO material draw improve
// TODO gen moves procedually? first captures
// TODO save game history and replay
// TODO king safety
//
// TODO shorter search bug fix
// TODO winning captures, equal captures, losing catpures last
// TODO bug bei wiederholung

// TODO program to edit polyglot files and sort them by hashkey
// TODO read from the book instead of loading everything in 
// TODO compatibility with arena
// TODO removed mintime temporariryl
// TODO fix reset board
// TODO outposted knights eval higher

int main(int argc, char** argv)
{
    
    int current_col = -1;

    S_PLAYER_PC player_pc[1];
    player_pc->plays_white = False;
    player_pc->info_white.depth = 7;
    player_pc->info_white.mintime = 10;
    player_pc->info_white.stop = 600;

    player_pc->plays_black = False;
    player_pc->info_black.depth = 7;
    player_pc->info_black.mintime = 10;
    player_pc->info_black.stop = 600;

    char* fen = (char*) START_FEN;

    for(int i = 0; i < argc; i++){
        if(argv[i][0] == '-'){
            if(argv[i][1] == 'c'){
                printf("computer col %s\n", argv[i+1]);
                if(argv[i+1][0] == 'w'){
                    current_col = WHITE;
                    player_pc->plays_white = True;
                }
                else if(argv[i+1][0] == 'b'){
                    current_col = BLACK;
                    player_pc->plays_black = True;
                }
            }
            if(argv[i][1] == 'd'){
                printf("computer depth %s\n", argv[i+1]);
                if(current_col == WHITE){
                    player_pc->info_white.depth = atoi(argv[i+1]);
                }
                else if(current_col == BLACK){
                    player_pc->info_black.depth = atoi(argv[i+1]);
                }
                else {
                    printf("d option without c option\n");
                    return 1;
                }
            }
            if(argv[i][1] == 't'){
                printf("computer time %s\n", argv[i+1]);
                if(current_col == WHITE){
                    player_pc->info_white.stop = atoi(argv[i+1]);
                }
                else if(current_col == BLACK) {
                    player_pc->info_black.stop = atoi(argv[i+1]);
                }
                else {
                    printf("t option without c option\n");
                    return 1;
                }
            }
            if(argv[i][1] == 'f'){
                fen = argv[i+1];
            }
        }
    }

    time_t t;
    srand((unsigned) time(&t));

    printf("%s\n", NAME);
    AllInit();
    
    S_BOARD pos;
    ResetBoard(&pos);

    S_MOVELIST list; 

    S_SEARCHINFO info[1];



    //PrintPieceBoard(&pos);
    
    Parse_Fen(fen, &pos);
    //PerftTest(6,&pos);
    //RandomTest(10000,&pos);
    
    //info->depth = 8;
    //info->stop = 300000;
    //iterdeep(&pos,info);
    
    //GUI
    gui(&pos,&list,player_pc);
    //Uci_Loop(&pos,info);
    printf("over main\n");
    return 0;
}
