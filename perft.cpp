#include "defs.hpp"
#include "stdio.h"

long leafNodes;

void Perft(int depth, S_BOARD *pos){

    ASSERT(CheckBoard(pos));
    
    if(depth == 0){
        leafNodes++;
        return;
    }

    S_MOVELIST list; //?
    list.count = 0;
    GenerateAllMoves(&list,pos);

    for(int i = 0;i < list.count;i++){
        if(pos->pieces64[FROMSQ(list.moves[i].move)] == EMPTY){
            PrintPieceBoard(pos);
            PrintBoardInfo(pos);
            printmv(list.moves[i].move);
            printf("\n%d\n",i);
        }
        ASSERT(pos->pieces[FROMSQ(list.moves[i].move)] != EMPTY);
    }

    int MoveNum = 0;
    for(MoveNum = 0;MoveNum < list.count;++MoveNum){
        
        S_MOVE move = list.moves[MoveNum];
        ASSERT(pos->pieces[FROMSQ(move.move)] != EMPTY);
        if( !MakeMove(pos,move.move)){
            // ???
            UndoMove(pos);
            continue;
        }

        Perft(depth-1,pos);
        UndoMove(pos);
    }

    return;

}

void PerftTest(int depth, S_BOARD *pos){

    PrintPieceBoard(pos);
    PrintBoardInfo(pos);
    printf("\nStarting Test to depth %d\n",depth);
    leafNodes = 0;
    
    S_MOVELIST list;
    list.count = 0;
    GenerateAllMoves(&list,pos);


    int MoveNum = 0;
    for(MoveNum = 0;MoveNum < list.count;++MoveNum){
        S_MOVE move = list.moves[MoveNum];
        if( !MakeMove(pos,move.move)){
            printf("undo because lul\n");
            UndoMove(pos); //??
            continue;
        }
        long cumnodes = leafNodes;
        Perft(depth-1,pos);
        UndoMove(pos);
        long oldnodes = leafNodes - cumnodes;
        printf("move %d : %ld ",MoveNum+1, oldnodes);
        printmv(move.move);
        printf("\n");
    }

    printf("\n Test complete: %ld nodes vidited\n",leafNodes);

    return;

}

void RandomTest(int times, S_BOARD* pos){
    int isover = 0; 
    int movecount = 0;
    for(int i = 0;i<times;i++){
        //make random move 
        ASSERT(CheckBoard(pos));
        RandomMove(pos);
        movecount++;

        isover = CheckIfOver(pos);
        if(isover){
            //undo all moves
            for(int j = 0;j<movecount;j++){
                UndoMove(pos);
            }
            printf("\nfinished with %d moves (following this be starting pos)\n",movecount);
            printfen(pos);
            PrintPieceBoard(pos);
            isover = 0;
            movecount = 0;
        }
    }
    printf("\nfinished all %d tests\n",times);
}
