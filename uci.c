// uci.c 
// stolen from vice
#include "stdio.h"
#include "defs.hpp"
#include "string.h"

#define INPUTBUFFER 400 * 6

// go depth 6 wtime 180000 btime 100000 binc 1000 winc 1000 movetime 1000 movestogo 40
void ParseGo(char* line, S_SEARCHINFO *info, S_BOARD *pos) {

	int depth = -1, movestogo = 30,movetime = -1;
	int time = -1, inc = 0;
    char *ptr = NULL;
	info->timeset = False;

	if ((ptr = strstr(line,"infinite"))) {
		;
	}

	if ((ptr = strstr(line,"binc")) && pos->side == BLACK) {
		inc = atoi(ptr + 5);
	}

	if ((ptr = strstr(line,"winc")) && pos->side == WHITE) {
		inc = atoi(ptr + 5);
	}

	if ((ptr = strstr(line,"wtime")) && pos->side == WHITE) {
		time = atoi(ptr + 6);
	}

	if ((ptr = strstr(line,"btime")) && pos->side == BLACK) {
		time = atoi(ptr + 6);
	}

	if ((ptr = strstr(line,"movestogo"))) {
		movestogo = atoi(ptr + 10);
	}

	if ((ptr = strstr(line,"movetime"))) {
		movetime = atoi(ptr + 9);
	}

	if ((ptr = strstr(line,"depth"))) {
		depth = atoi(ptr + 6);
	}

	if(movetime != -1) {
		time = movetime;
		movestogo = 1;
	}

	info->start = GetTimeMs();
	info->depth = depth;

	if(time != -1) {
		info->timeset = True;
		time /= movestogo;
		time -= 50;
		info->stop =  time + inc;
	}

	if(depth == -1) {
		info->depth = MAXDEPTH;
	}

	printf("time:%d start:%d stop:%d depth:%d timeset:%d\n",
		time,info->start,info->stop,info->depth,info->timeset);
	iterdeep(pos, info);
}

// position fen fenstr
// position startpos
// ... moves e2e4 e7e5 b7b8q
void ParsePosition(char* lineIn, S_BOARD *pos) {
    printf("PARSE POSITION\n\n\n");
	lineIn += 9;
    char *ptrChar = lineIn;

    if(strncmp(lineIn, "startpos", 8) == 0){
        char* fen = (char*) START_FEN;
        printf("START POSITION\n\n\n");
        Parse_Fen(fen, pos);
    } else {
        printf("NOT START POSITION\n\n\n");
        ptrChar = strstr(lineIn, "fen");
        if(ptrChar == NULL) {
            char* fen = (char*) START_FEN;
            Parse_Fen(fen, pos);
        } else {
            ptrChar+=4;
            Parse_Fen((char*)ptrChar, pos);
        }
    }
    printf("before move\n");
	PrintPieceBoard(pos);
    PrintBoardInfo(pos);

	ptrChar = strstr(lineIn, "moves");
	int move;

	if(ptrChar != NULL) {
        ptrChar += 6;
        while(*ptrChar) {
              move = ParseMove(ptrChar,pos);
			  if(move == NOMOVE) break;
			  MakeMove(pos, move);
              pos->ply=0;
              while(*ptrChar && *ptrChar!= ' ') ptrChar++;
              ptrChar++;
        }
    }
	PrintPieceBoard(pos);
    PrintBoardInfo(pos);
}

void Uci_Loop(S_BOARD *pos, S_SEARCHINFO *info) {

	//info->GAME_MODE = UCIMODE;
    int MAX_HASH = 1024;

	setbuf(stdin, NULL);
    setbuf(stdout, NULL);

	char line[INPUTBUFFER];
    printf("id name %s\n",NAME);
    printf("id author me\n");
	printf("option name Hash type spin default 64 min 4 max %d\n",MAX_HASH);
	printf("option name Book type check default true\n");
    printf("uciok\n");
	
	int MB = 64;

	while (True) {
		memset(&line[0], 0, sizeof(line));
        fflush(stdout);
        if (!fgets(line, INPUTBUFFER, stdin))
        continue;

        if (line[0] == '\n')
        continue;

        printf("line: ... %s",line);

        if (!strncmp(line, "isready", 7)) {
            printf("readyok\n");
            continue;
        } else if (!strncmp(line, "position", 8)) {
            printf("position!\n");
            ParsePosition(line, pos);
        } else if (!strncmp(line, "ucinewgame", 10)) {
            printf("ucinewgame!\n");
            ParsePosition((char*)"position startpos\n", pos);
        } else if (!strncmp(line, "go", 2)) {
            printf("Seen Go..\n");
            ParseGo(line, info, pos);
        } else if (!strncmp(line, "quit", 4)) {
            info->quit = True;
            break;
        } else if (!strncmp(line, "uci", 3)) {
            printf("id name %s\n",NAME);
            printf("id author me\n");
            printf("uciok\n");
        //TODO implement
        //} else if (!strncmp(line, "debug", 4)) {
        //    DebugAnalysisTest(pos,info);
        //    break;
        } else if (!strncmp(line, "setoption name Hash value ", 26)) {			
			sscanf(line,"%*s %*s %*s %*s %d",&MB);
			if(MB < 4) MB = 4;
			if(MB > MAX_HASH) MB = MAX_HASH;
			printf("Set Hash to %d MB\n",MB);
			InitHashTable(pos->HashTable, MB);
		} else if (!strncmp(line, "setoption name Book value ", 26)) {			
			char *ptrTrue = NULL;
			ptrTrue = strstr(line, "true");
            //TODO implement
			//if(ptrTrue != NULL) {
			//	EngineOptions->UseBook = True;
			//} else {
			//	EngineOptions->UseBook = False;
			//}
		}
		if(info->quit) break;
    }
}













