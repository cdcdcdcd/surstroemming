#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "defs.hpp"
//#include <unistd.h>

#define flip_coord(x) ( 7*SIZE/8 - (x) )
#define flip_sq(x) (FLIP ? FR2SQ64(FILESQ64((x)),7 - (RANKSQ64((x)))) : (x)  )
int FLIP = False;
//GUI
typedef struct images {
    SDL_Texture* wp;
    SDL_Texture* wn;
    SDL_Texture* wr;
    SDL_Texture* wq;
    SDL_Texture* wk;
    SDL_Texture* wb;
    SDL_Texture* bp;
    SDL_Texture* bn;
    SDL_Texture* br;
    SDL_Texture* bq;
    SDL_Texture* bk;
    SDL_Texture* bb;
} S_IMAGES;

typedef struct  {
    int from;
    int to;
    int selected;
    int mousex;
    int mousey;
    int isdragging;
} S_SQUAREINFO;


void initimg(S_IMAGES* images,SDL_Renderer* renderer){
    SDL_Surface* image;

    image = IMG_Load("./images/wP.svg.png");
    if(!image) printf("error loading wp\n");
    images->wp = SDL_CreateTextureFromSurface(renderer,image);

    image = IMG_Load("./images/wN.svg.png");
    if(!image) printf("error loading wn\n");
    images->wn = SDL_CreateTextureFromSurface(renderer,image);

    image = IMG_Load("./images/wB.svg.png");
    if(!image) printf("error loading wb\n");
    images->wb = SDL_CreateTextureFromSurface(renderer,image);

    image = IMG_Load("./images/wR.svg.png");
    if(!image) printf("error loading wr\n");
    images->wr = SDL_CreateTextureFromSurface(renderer,image);

    image = IMG_Load("./images/wQ.svg.png");
    if(!image) printf("error loading wq\n");
    images->wq = SDL_CreateTextureFromSurface(renderer,image);

    image = IMG_Load("./images/wK.svg.png");
    if(!image) printf("error loading wk\n");
    images->wk = SDL_CreateTextureFromSurface(renderer,image);


    image = IMG_Load("./images/bP.svg.png");
    if(!image) printf("error loading bp\n");
    images->bp = SDL_CreateTextureFromSurface(renderer,image);

    image = IMG_Load("./images/bN.svg.png");
    if(!image) printf("error loading bn\n");
    images->bn = SDL_CreateTextureFromSurface(renderer,image);

    image = IMG_Load("./images/bB.svg.png");
    if(!image) printf("error loading wb\n");
    images->bb = SDL_CreateTextureFromSurface(renderer,image);

    image = IMG_Load("./images/bR.svg.png");
    if(!image) printf("error loading br\n");
    images->br = SDL_CreateTextureFromSurface(renderer,image);

    image = IMG_Load("./images/bQ.svg.png");
    if(!image) printf("error loading bq\n");
    images->bq = SDL_CreateTextureFromSurface(renderer,image);

    image = IMG_Load("./images/bK.svg.png");
    if(!image) printf("error loading bk\n");
    images->bk = SDL_CreateTextureFromSurface(renderer,image);

    SDL_FreeSurface(image);
}

void draw_piece(SDL_Renderer* renderer,S_IMAGES* images, S_BOARD* pos,SDL_Rect* rect,int pce){
    switch (pce){
        case wp: 
            SDL_RenderCopy(renderer, images->wp, NULL, rect);
            break;
        case wn: 
            SDL_RenderCopy(renderer, images->wn, NULL, rect);
            break;
        case wb: 
            SDL_RenderCopy(renderer, images->wb, NULL, rect);
            break;
        case wr: 
            SDL_RenderCopy(renderer, images->wr, NULL, rect);
            break;
        case wk: 
            SDL_RenderCopy(renderer, images->wk, NULL, rect);
            break;
        case wq: 
            SDL_RenderCopy(renderer, images->wq, NULL, rect);
            break;
        case bp: 
            SDL_RenderCopy(renderer, images->bp, NULL, rect);
            break;
        case bn: 
            SDL_RenderCopy(renderer, images->bn, NULL, rect);
            break;
        case bb: 
            SDL_RenderCopy(renderer, images->bb, NULL, rect);
            break;
        case br: 
            SDL_RenderCopy(renderer, images->br, NULL, rect);
            break;
        case bk: 
            SDL_RenderCopy(renderer, images->bk, NULL, rect);
            break;
        case bq: 
            SDL_RenderCopy(renderer, images->bq, NULL, rect);
            break;
        default:
            break;
    }
}

void draw(SDL_Renderer* renderer,S_IMAGES* images, S_BOARD* pos,SDL_Rect* rect,S_SQUAREINFO* visual) { // get pos and then draw it
    rect->w = SIZE/8; //1/8 of size
    rect->h = SIZE/8;
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
	SDL_RenderClear(renderer);
    int sqsize = SIZE / 8;
	int i, j;
    int _i, _j;
    int sq;
	for (i = 0; i < SIZE; ++i) for(j = 0; j < SIZE; ++j){
        _i = i / sqsize; 
        _j = j / sqsize; 
        sq = flip_sq((_i+8*(7-_j)));
        if((sq == visual->from || sq == visual->to)&&(_i+_j)%2==0) SDL_SetRenderDrawColor(renderer, 0xCB, 0xD0, 0x6A, 0);
        else if(sq == visual->from || sq == visual->to) SDL_SetRenderDrawColor(renderer, 0xA8, 0xA2, 0x3C, 0);
        else if((_i+_j)%2 == 0) SDL_SetRenderDrawColor(renderer, 0xF0, 0xD9, 0xB5, 0);
        else SDL_SetRenderDrawColor(renderer, 0xB5, 0x88, 0x63, 0);
		SDL_RenderDrawPoint(renderer, i, j);
    }

    //render images
    int dragx;
    int dragy;
    int dragpce;
    for (i = 0; i < 8; ++i) for(j = 0; j < 8; j++){
        if(flip_sq((i+8*(7-j))) == visual->selected && visual->isdragging){
            dragx = visual->mousex-sqsize/2;
            dragy = visual->mousey-sqsize/2;
            dragpce = pos->pieces64[flip_sq((i+8*(7-j)))];
            continue;
        }
        rect->x = i*sqsize;
        rect->y = j*sqsize;
        int pce = pos->pieces64[flip_sq((i+8*(7-j)))];
        draw_piece(renderer,images,pos,rect,pce);
    }
    //render dragged piece
    rect->x = dragx;
    rect->y = dragy;
    draw_piece(renderer,images,pos,rect,dragpce);
}

void gui(S_BOARD* pos,S_MOVELIST* list, S_PLAYER_PC* player_pc){

    int value = 0;

    int clrow;
    int clcol;

    int clsq; //calculated from clrow, clcol
                    
    int clfrom = 99;
    int clto = 99;

    int over = False;

    S_SQUAREINFO visual[1];
    visual->from = NO_SQ;
    visual->to = NO_SQ;
    visual->selected = NO_SQ;
    visual->isdragging = False;

    int bestmove;
	int running = 1;
    int sqsize = SIZE / 8;

    int computerthinking = 0;

	Uint32 start;

	SDL_Event event;
	SDL_Renderer* renderer;

	SDL_Window* win;

	SDL_Init(SDL_INIT_VIDEO);
	SDL_CreateWindowAndRenderer(SIZE, SIZE, 0, &win, &renderer);
    IMG_Init(IMG_INIT_PNG); //
    S_IMAGES images;
    initimg(&images,renderer);

    SDL_Rect rect;

    //TODO command line arg
	InitHashTable(pos->HashTable, 100000);

	while(running)
	{
		start = SDL_GetTicks();
        SDL_GetMouseState(&visual->mousex,&visual->mousey);
        clcol = visual->mousex / sqsize;
        clrow = 7-visual->mousey / sqsize;
		while(SDL_PollEvent(&event)){
			switch(event.type)
				{
				 case SDL_QUIT:
					 running = 0;
					 break;
				 case SDL_KEYDOWN:
					 switch (event.key.keysym.sym)
						{
								case SDLK_q: running = 0; break;
								case SDLK_i: FLIP = !FLIP; break;
								case SDLK_o: over = !over; break;
								case SDLK_r: RandomMove(pos); break;
								case SDLK_u: 
                                    if(!computerthinking){
                                        UndoMove(pos); 
                                        UndoMove(pos); 
                                    }
                                    break;
						}
                 case SDL_MOUSEBUTTONUP:
                    switch (event.button.button)
                    {
                        case SDL_BUTTON_LEFT:
                            clsq = clcol+clrow*8;
                            clsq = flip_sq(clsq);
                            clto = clsq;
                            visual->selected = NO_SQ;
                            visual->isdragging = False;
                            if(!InputMove(pos, clfrom,clto)){
                                clfrom = clsq;
                            }else{ //sucess
                                printf("what is sucess\n");
                                visual->from = clfrom; 
                                visual->to = clto; 
                            }
                            break;
                    }
                    break;
                 case SDL_MOUSEBUTTONDOWN:
                    switch (event.button.button)
                    {
                        case SDL_BUTTON_LEFT:
                            if(!computerthinking){
                                clsq = clcol+clrow*8;
                                clsq = flip_sq(clsq);
                                visual->selected = clsq;
                                printsq64(clsq);
                                visual->isdragging = True;
                                clfrom = clsq; 
                            }
                            break;
                        case SDL_BUTTON_RIGHT:
                            GenerateAllMoves(list,pos);
                            printmvlist(list);
                            printf("num : %d\n",list->count);
                            printf("eval %d\n",EvalPosition(pos));
                            break;
                        default:
                            printf("Some mousebutton was pressed\n");
                            break;
                    }
                    break;
				 }
		}
		draw(renderer,&images,pos,&rect,visual);
	    SDL_RenderPresent(renderer);

        ASSERT(CheckBoardNew(pos));
        // check if its over
        over = CheckIfOver(pos);
        if(value < -ISMATE) over = GIVEUP; // give up
        if(over == CHECKMATED) printf("CHECKMATED\n");
        if(over == DRAW) printf("DRAW\n");
        if(over == GIVEUP) printf("GIVEUP\n");
        //if(value < -MATE + 6) over = CHECKMATED;


        if(!over){
            if(((pos->side == WHITE && player_pc->plays_white) || (pos->side == BLACK && player_pc->plays_black)) && !computerthinking){
                computerthinking = 1;
                if (pos->side == WHITE){
                    bestmove = iterdeep(pos,  &player_pc->info_white); //TODO info
                }
                else
                {
                    bestmove = iterdeep(pos,  &player_pc->info_black); //TODO info
                }
                printmv(bestmove);
                visual->from = FROMSQ(bestmove);
                visual->to = TOSQ(bestmove);
                computerthinking = 0;
                PrintPieceBoard(pos);
                printf("thought\n");
                
            }
        }

		if(1000/FPS > (SDL_GetTicks() -start))
			SDL_Delay(1000/FPS - (SDL_GetTicks() -start));
	}	

    printf("over\n");

    IMG_Quit();

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(win);
	SDL_Quit();
}
