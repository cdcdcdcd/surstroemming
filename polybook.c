#include "defs.hpp"
#include "polykeys.h"
#include <stdio.h>

unsigned short endian_swap_u16(unsigned short x) 
{ 
    x = (x>>8) | 
        (x<<8); 
    return x;
} 

unsigned int endian_swap_u32(unsigned int x) 
{ 
    x = (x>>24) | 
        ((x<<8) & 0x00FF0000) | 
        ((x>>8) & 0x0000FF00) | 
        (x<<24); 
    return x;
} 

U64 endian_swap_u64(U64 x) 
{ 
    x = (x>>56) | 
        ((x<<40) & 0x00FF000000000000) | 
        ((x<<24) & 0x0000FF0000000000) | 
        ((x<<8)  & 0x000000FF00000000) | 
        ((x>>8)  & 0x00000000FF000000) | 
        ((x>>24) & 0x0000000000FF0000) | 
        ((x>>40) & 0x000000000000FF00) | 
        (x<<56); 
    return x;
}


typedef struct { //ordering important!!
	U64 key;
	unsigned short move;
	unsigned short weight;
	unsigned int learn;
} S_POLY_BOOK_ENTRY;

long NumEntries = 0;

S_POLY_BOOK_ENTRY *entries;

const int PolyKindOfPiece[13] = {
	-1, 1, 3, 5, 7, 9, 11, 0, 2, 4, 6, 8, 10
};

int ConvertPolyMoveToInternalMove(unsigned short polyMove, S_BOARD *pos) {
	
	int ff = (polyMove >> 6) & 7;
	int fr = (polyMove >> 9) & 7;
	int tf = (polyMove >> 0) & 7;
	int tr = (polyMove >> 3) & 7;
	int pp = (polyMove >> 12) & 7;

    int side = pos->side;

    int prom = side ? bq : wq;
    if(pp == 1) prom = side ? bn : wn;
    if(pp == 2) prom = side ? bb : wb;
    if(pp == 3) prom = side ? br : wr;

    int move = InputToMove(pos, FR2SQ64(ff,fr), FR2SQ64(tf,tr), prom);
    return move;
}


void InitPolyBook() {

	//EngineOptions->UseBook = False;

	FILE *fp = fopen("baron30.bin","rb");

	if(fp == NULL) {
		printf("Book File Not Read\n");
	} else {
		fseek(fp,0,SEEK_END);
		long position = ftell(fp);
		
		if(position < sizeof(S_POLY_BOOK_ENTRY)) {
			printf("No Entries Found\n");
			return;
		}
		
		NumEntries = position / sizeof(S_POLY_BOOK_ENTRY);
		printf("%ld Entries Found In File\n", NumEntries);
		
		//entries = (S_POLY_BOOK_ENTRY*)malloc(NumEntries * sizeof(S_POLY_BOOK_ENTRY));
		//rewind(fp);
		//
		//size_t returnValue;
		//returnValue = fread(entries, sizeof(S_POLY_BOOK_ENTRY), NumEntries, fp);
		//printf("fread() %ld Entries Read in from file\n", returnValue);
		
		//if(NumEntries > 0) {
		//	EngineOptions->UseBook = TRUE;
		//}
	}
    printf("Poly init complete\n");
}

void CleanPolyBook() {
	free(entries);
}

int HasPawnForCapture(const S_BOARD *board) {
	int sqWithPawn = 0;
	int p = (board->side == WHITE) ? wp : bp;
	if(board->enPas != NO_SQ) {
		if(board->side == WHITE) {
			sqWithPawn = board->enPas + S;
		} else {
			sqWithPawn = board->enPas + N;
		}
		
		if(board->pieces64[sqWithPawn + W] == p) {
			return True;
		} else if(board->pieces64[sqWithPawn + E] == p) {
			return True;
		} 
	}
	return False;
}

U64 PolyKeyFromBoard(const S_BOARD *board) {

	int sq = 0, rank = 0, file = 0;
	U64 finalKey = 0;
	int piece = EMPTY;
	int polyPiece = 0;
	int offset = 0;
	
	for(sq = 0; sq < 64; ++sq) {
		piece = board->pieces64[sq];
		if(piece != EMPTY) {
			polyPiece = PolyKindOfPiece[piece];
			rank = RANKSQ64(sq);
			file = FILESQ64(sq);
			finalKey ^= Random64Poly[(64 * polyPiece) + (8 * rank) + file];
		}
	}
	
	// castling
	offset = 768;
	
	if(board->castlePerm & WKC) finalKey ^= Random64Poly[offset + 0];
	if(board->castlePerm & WQC) finalKey ^= Random64Poly[offset + 1];
	if(board->castlePerm & BKC) finalKey ^= Random64Poly[offset + 2];
	if(board->castlePerm & BQC) finalKey ^= Random64Poly[offset + 3];
	
	// enpassant
	offset = 772;
	if(HasPawnForCapture(board)) {
		file = FILESQ64(board->enPas);
		finalKey ^= Random64Poly[offset + file];
	}
	
	if(board->side == WHITE) {
		finalKey ^= Random64Poly[780];
	}
	return finalKey;
}

int GetBookMoveBinary(S_BOARD* pos){
    printf("getbook\n");
	FILE *fp = fopen("baron30.bin","rb");

    S_POLY_BOOK_ENTRY entry;

	int tempMove = NOMOVE;
    int size = sizeof(S_POLY_BOOK_ENTRY);

	U64 polyKey = PolyKeyFromBoard(pos);

    int upper = NumEntries;
    int lower = 0;
    int svalue;

	const int MAXBOOKMOVES = 32;
	int bookMoves[MAXBOOKMOVES];

    while(lower <= upper){
        svalue = (upper + lower)/2;
        fseek(fp,size*svalue, SEEK_SET);
        fread(&entry, size, 1, fp); 
        if(polyKey == endian_swap_u64(entry.key)){
            break;
        }
        else if(polyKey > endian_swap_u64(entry.key)){
            lower = svalue+1; 
            rewind(fp);
        }
        else{
            upper = svalue-1;
            rewind(fp);
        }
    }

    int count = 0;
    int value = svalue;
    printf("svalue %d\n",svalue);
    //seek forward until unequal
    while(endian_swap_u64(entry.key) == polyKey){
	    int move = endian_swap_u16(entry.move);

		tempMove = ConvertPolyMoveToInternalMove(move, pos);
		if(tempMove != NOMOVE) {
			bookMoves[count++] = tempMove;
			if(count > MAXBOOKMOVES) break;
		}

        value++;

        fseek(fp,size*value, SEEK_SET);
        fread(&entry, size, 1, fp); 
        printf("\n");
    }

    //printf("upper %d\n",value);
    int max = value;

    value = svalue-1;
    //seek backward until unequal
    while(endian_swap_u64(entry.key) == polyKey){
	    int move = endian_swap_u16(entry.move);

		tempMove = ConvertPolyMoveToInternalMove(move, pos);
		if(tempMove != NOMOVE) {
			bookMoves[count++] = tempMove;
			if(count > MAXBOOKMOVES) break;
		}

        value--;

        fseek(fp,size*value, SEEK_SET);
        fread(&entry, size, 1, fp); 
        printf("\n");
    }
    printf("lower %d\n",value);
    int min = value;

    tempMove = bookMoves[rand() % (max - min)];

    fclose(fp);

    return tempMove;
}

//TODO delete

int GetBookMove(S_BOARD *pos) {
	int index = 0;
	S_POLY_BOOK_ENTRY *entry;
	unsigned short move;
	const int MAXBOOKMOVES = 32;
	int bookMoves[MAXBOOKMOVES];
	int tempMove = NOMOVE;
	int count = 0;
	
	U64 polyKey = PolyKeyFromBoard(pos);

	for(entry = entries; entry < entries + NumEntries; entry++) {
		if(polyKey == endian_swap_u64(entry->key)) {
			move = endian_swap_u16(entry->move);

			tempMove = ConvertPolyMoveToInternalMove(move, pos);
			if(tempMove != NOMOVE) {
				bookMoves[count++] = tempMove;
				if(count > MAXBOOKMOVES) break;
			}
		}
	}
	
	if(count != 0) {
		int randMove = rand() % count;
		return bookMoves[randMove];
	} else {
		return NOMOVE;
	}
}

//void PrintMovesOrdered(){
//	S_POLY_BOOK_ENTRY *entry;
//    U64 key;
//    U64 prevkey = 0ULL;
//    int min;
//
//	for(entry = entries; entry < entries + NumEntries; entry++) {
//		key = endian_swap_u64(entry->key);
//        if(prevkey > key){
//            printf("this is not ordered\n");
//        }
//        prevkey = key;
//	}
//}
