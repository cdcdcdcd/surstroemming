// pvtable.c

#include "stdio.h"
#include "defs.hpp"

int GetPvLine(const int depth, S_BOARD *pos) {

	ASSERT(depth < MAXDEPTH);

	int move = GetMovePvTable(pos);
	int count = 0;
	
	while(move != NOMOVE && count < depth) {
	
		ASSERT(count < MAXDEPTH);
	
		if( MoveExists(pos, move) ) {
			MakeMove(pos, move);
			pos->PvArray[count++] = move;
		} else {
			break;
		}		
		move = GetMovePvTable(pos);
	}
	
	while(pos->ply > 0) {
		UndoMove(pos);
	}
	
	return count;
	
}

int GetMovePvTable(const S_BOARD *pos) {

	int index = pos->posKey % pos->HashTable->numEntries; //changed to pos->hashtable
	//ASSERT(index >= 0 && index <= pos->PvTable->numEntries - 1);
	
	if( pos->HashTable->pTable[index].posKey == pos->posKey ) {
		return pos->HashTable->pTable[index].move; //TODO changed from PvTable to HashTable
	}
	
	return NOMOVE;
}

// HASH
void ClearHashTable(S_HASHTABLE *table) {

  S_HASHENTRY *tableEntry;
  
  for (tableEntry = table->pTable; tableEntry < table->pTable + table->numEntries; tableEntry++) {
    tableEntry->posKey = 0ULL;
    tableEntry->move = NOMOVE;
    tableEntry->depth = 0;
    tableEntry->score = 0;
    tableEntry->flags = 0;
  }
  table->newWrite=0;
}

void InitHashTable(S_HASHTABLE *table, const int MB) {  
	
	int HashSize = 0x100000 * MB;
    table->numEntries = HashSize / sizeof(S_HASHENTRY);
    table->numEntries -= 2;
		
    table->pTable = (S_HASHENTRY *) malloc(table->numEntries * sizeof(S_HASHENTRY));
	if(table->pTable == NULL) {
		printf("Hash Allocation Failed, trying %dMB...\n",MB/2);
		InitHashTable(table,MB/2);
	} else {
		ClearHashTable(table);
		printf("HashTable init complete with %d entries\n",table->numEntries);
	}
	
}

int ProbeHashEntry(S_BOARD *pos, int *move, int *score, int alpha, int beta, int depth) {

    //printf("num entries %d \n",pos->HashTable->numEntries);
	int index = pos->posKey % pos->HashTable->numEntries;
	
	ASSERT(index >= 0 && index <= pos->HashTable->numEntries - 1);
    ASSERT(depth>=1&&depth<MAXDEPTH);
    ASSERT(alpha<beta);
    ASSERT(alpha>=-INFINITE&&alpha<=INFINITE);
    ASSERT(beta>=-INFINITE&&beta<=INFINITE);
    ASSERT(pos->ply>=0&&pos->ply<MAXDEPTH);
	
	if( pos->HashTable->pTable[index].posKey == pos->posKey ) {
		*move = pos->HashTable->pTable[index].move;
		if(pos->HashTable->pTable[index].depth >= depth){
			pos->HashTable->hit++;
			
			ASSERT(pos->HashTable->pTable[index].depth>=1&&pos->HashTable->pTable[index].depth<MAXDEPTH);
            ASSERT(pos->HashTable->pTable[index].flags>=HFALPHA&&pos->HashTable->pTable[index].flags<=HFEXACT);
			
			*score = pos->HashTable->pTable[index].score;
			if(*score > ISMATE) *score -= pos->ply;
            else if(*score < -ISMATE) *score += pos->ply;
			
			switch(pos->HashTable->pTable[index].flags) {
				
                ASSERT(*score>=-INFINITE&&*score<=INFINITE);

                case HFALPHA: if(*score<=alpha) {
                    *score=alpha;
                    return True;
                    }
                    break;
                case HFBETA: if(*score>=beta) {
                    *score=beta;
                    return True;
                    }
                    break;
                case HFEXACT:
                    return True;
                    break;
                default: ASSERT(False); break;
            }
		}
	}
	
	return False;
}

void StoreHashEntry(S_BOARD *pos, const int move, int score, const int flags, const int depth) {

	int index = pos->posKey % pos->HashTable->numEntries;
	
	ASSERT(index >= 0 && index <= pos->HashTable->numEntries - 1);
	ASSERT(depth>=1&&depth<MAXDEPTH);
    ASSERT(flags>=HFALPHA&&flags<=HFEXACT);
    ASSERT(score>=-INF&&score<=INF);
    ASSERT(pos->ply>=0&&pos->ply<MAXDEPTH);
	
	if( pos->HashTable->pTable[index].posKey == 0) {
		pos->HashTable->newWrite++;
	} else {
		pos->HashTable->overWrite++;
	}
	
	if(score > ISMATE) score += pos->ply;
    else if(score < -ISMATE) score -= pos->ply;
	
	pos->HashTable->pTable[index].move = move;
    pos->HashTable->pTable[index].posKey = pos->posKey;
	pos->HashTable->pTable[index].flags = flags;
	pos->HashTable->pTable[index].score = score;
	pos->HashTable->pTable[index].depth = depth;
}

//const int PvSize = 0x100000 * 2;
//
//void ClearPvTable(S_PVTABLE *table) {
//
//  S_PVENTRY *pvEntry;
//
//  for (pvEntry = table->pTable; pvEntry < table->pTable + table->numEntries; pvEntry++) {
//    pvEntry->posKey = 0ULL;
//    pvEntry->move = NOMOVE;
//  }
//}
//
//void InitPvTable(S_PVTABLE *table) {  
//  
//    table->numEntries = PvSize / sizeof(S_PVENTRY);
//    table->numEntries -= sizeof(S_PVENTRY)*2;
//    //free(table->pTable); //if you didnt malloc it you cant free it -stackoverflow
//    table->pTable = (S_PVENTRY *) malloc(table->numEntries * sizeof(S_PVENTRY));
//    ClearPvTable(table);
//    printf("PvTable init complete with %d entries\n",table->numEntries);
//	
//}
//
//void StorePvMove(const S_BOARD *pos, const int move) {
//
//	int index = pos->posKey % pos->PvTable->numEntries;
//	ASSERT(index >= 0 && index <= pos->PvTable->numEntries - 1);
//	
//	pos->PvTable->pTable[index].move = move;
//    pos->PvTable->pTable[index].posKey = pos->posKey;
//}
