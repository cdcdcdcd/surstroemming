#include	"defs.hpp"
#include "stdio.h"

#define HASH_PCE(pce,sq) (pos->posKey ^= (PieceKeys[(pce)][(sq)]))
#define HASH_CA (pos->posKey ^= (CastleKeys[(pos->castlePerm)]))
#define HASH_SIDE (pos->posKey ^= (SideKey))
#define HASH_EP (pos->posKey ^= (PieceKeys[EMPTY][(pos->enPas)]))

static void RemovePiece(const int sq, S_BOARD *pos) {
    //printf("rem piece\n");
    int piece = pos->pieces64[sq];
    int col = colList[piece];
    //clear from board
    pos->pieces64[sq] = EMPTY;
    //hash
    HASH_PCE(piece,sq);

    //clear from plist
    int t;

    //clear from pawnbit
    //decrement maj min big
    CLRBIT(pos->piecebb[piece],sq);
    CLRBIT(pos->occu[col],sq);
    CLRBIT(pos->occu[BOTH],sq);

    if(!isBig[piece]) CLRBIT(pos->allpawns,sq);

    //adjust material list
    pos->material[colList[piece]] -= valList[piece];	
}


static void AddPiece(const int sq, S_BOARD *pos, const int pce) {
    //printf("add piece\n");
    int col = colList[pce];
    //Add to board
    pos->pieces64[sq] = pce;
    //Hash piece
    HASH_PCE(pce,sq);
    //add to pList
    //add pawnbit
    //increment maj min big
    SETBIT(pos->piecebb[pce], sq);
    SETBIT(pos->occu[col],sq);
    SETBIT(pos->occu[BOTH],sq);

    //TODO pawn stuff
    if(!isBig[pce]) SETBIT(pos->allpawns,sq);
    //adjust material list
    pos->material[colList[pce]] += valList[pce];	
}

static void MovePiece(const int from, const int to, S_BOARD *pos) {
    //printf("\nmove piece\n");
    int piece = pos->pieces64[from];
    int topiece = pos->pieces64[to];
    int col = colList[piece];//not always the same as pos->side
    int xcol = colList[topiece];//not always the same as pos->side
    // move the piece to square
    pos->pieces64[to] = piece;
    pos->pieces64[from] = EMPTY;
    //hash
    HASH_PCE(piece,from);
    HASH_PCE(piece,to);

    CLRBIT(pos->occu[col],from);
    CLRBIT(pos->occu[BOTH],from);

    SETBIT(pos->occu[BOTH],to);
    SETBIT(pos->occu[col],to);
    if(topiece != EMPTY) CLRBIT(pos->occu[xcol],to); //TODO improve

    CLRBIT(pos->piecebb[topiece],to);
    CLRBIT(pos->piecebb[piece],from);
    SETBIT(pos->piecebb[piece],to);
    //delete pawnbit at from and then add pawnbit at to
    //do not increment maj min big
	if(!isBig[piece]) { //
		CLRBIT(pos->allpawns,from);
		SETBIT(pos->allpawns,to);
	}
    //adjust material list
    //printf("move piece end\n");
}


int MakeMove(S_BOARD *pos, int mv) {
    //printmvl(mv);

    //if(!CheckBoardNew(pos)) exit(0);
    //PrintPieceBoard(pos);
    //PrintBoardInfo(pos);
    
    //ASSERT(CheckBoard(pos));

	int from = FROMSQ(mv);
    int to = TOSQ(mv);
    int side = pos->side;
	
    int cap = CAPTURED(mv);
    int pwnstrt = PAWNSTART(mv);
    int prompce = PROMOTED(mv);
    int enpas = ENPAS(mv);
    int castle = CASTLE(mv);
    
    int movedpc = pos->pieces64[from];

    //update history strcuture move, fiftymove, enpas, castleperm
    pos->history[pos->hisPly].move = mv;
    pos->history[pos->hisPly].fiftyMove = pos->fiftyMove;
    pos->history[pos->hisPly].enPas = pos->enPas;
    pos->history[pos->hisPly].castlePerm = pos->castlePerm;
	pos->history[pos->hisPly].posKey = pos->posKey;
    //move the piece from -> to (to in promotions)
    pos->fiftyMove++; 
    //reset if capture
    if(movedpc == wp || movedpc == bp || cap) pos->fiftyMove = 0;
    
    //remove enpas if there is no enpas
	if(pos->enPas != NO_SQ) HASH_EP;

    //Case 1: normal move to empty square
    if(!pwnstrt && !cap && !enpas && !castle && !prompce){
        // Move piece
        MovePiece(from, to, pos);
    } 
    //Case 2: pawnstart move to empty square (pwnstrt) ASSERT rest is not true
    if(pwnstrt){
        // Move piece
        MovePiece(from, to, pos);
        // set enpas square
        pos->enPas = (from + to)>>1;
        //set new enpas
        HASH_EP;

    } else {
        pos->enPas = NO_SQ;
    }
    
    //Case 3: normal capture move (cap)
    if(cap && !enpas && !prompce){
        // remove piece 
        RemovePiece(to,pos);
        // move piece
        MovePiece(from, to, pos);

    } 
    
    //Case 4: enpassant capture move (cap && enpas) (enpas implies cap)
    if(enpas){
        int dir = side ? N : S;
        // remove piece from weird place
        RemovePiece(to+dir,pos);
        // move piece to enpas square
        MovePiece(from,to,pos); //to == enpas square
    } 
    
    //Case 5: castle (castle)
    if(castle){
        // move king to to
        MovePiece(from,to,pos);
        int ROOKRANK = side ? RANK_8 : RANK_1;
        if(FILESQ64(to) == FILE_G){
            // move rook from G,rookrank to left of F
            MovePiece(FR2SQ64(FILE_H,ROOKRANK),FR2SQ64(FILE_F,ROOKRANK),pos);
        }
        else if(FILESQ64(to) == FILE_C){
            // move rook from C,rookrank to D
            MovePiece(FR2SQ64(FILE_A,ROOKRANK),FR2SQ64(FILE_D,ROOKRANK),pos);
        }
    } 
    
    //Case 6: promotion (prompce) but no capture
    if(!cap && prompce){
        //remove pawn
        RemovePiece(from,pos);
        //add the new piece
        AddPiece(to,pos,prompce);
    } 

    //Case 7: promotion (prompce) with capture
    if(cap && prompce){
        //remove pawn
        RemovePiece(from,pos);
        //remove captured piece
        RemovePiece(to,pos);
        //add the new piece
        AddPiece(to,pos,prompce);
    } 
    //remove castling
    HASH_CA;
    // update castle rights TODO improve to == FR2SQ64(f,r)??
    if(((FILESQ64(to) == FILE_A && RANKSQ64(to) == RANK_1)|| movedpc == wk || (movedpc == wr && FILESQ64(from) == FILE_A)) && (pos->castlePerm & WQC)) pos->castlePerm -= WQC;
    if(((FILESQ64(to) == FILE_A && RANKSQ64(to) == RANK_8)|| movedpc == bk || (movedpc == br && FILESQ64(from) == FILE_A)) && (pos->castlePerm & BQC)) pos->castlePerm -= BQC;
    if(((FILESQ64(to) == FILE_H && RANKSQ64(to) == RANK_1)|| movedpc == wk || (movedpc == wr && FILESQ64(from) == FILE_H)) && (pos->castlePerm & WKC)) pos->castlePerm -= WKC;
    if(((FILESQ64(to) == FILE_H && RANKSQ64(to) == RANK_8)|| movedpc == bk || (movedpc == br && FILESQ64(from) == FILE_H)) && (pos->castlePerm & BKC)) pos->castlePerm -= BKC;
	//add castlign	
    HASH_CA;

    pos->ply++;
    pos->hisPly++;

    pos->side = !pos->side;
    HASH_SIDE;
    
    //if(GeneratePosKey(pos)!=pos->posKey){ //TODO comment out
    //    printf("why fail\n");
    //    printmvl(mv);
    //    exit(1);
    //}
	ASSERT(GeneratePosKey(pos)==pos->posKey);

    int sq = lsb(pos->piecebb[side ? bk : wk]); //TODO use bitboard
    if(BitSqAttacked(sq,side,pos)){  //TODO change back to bit version
        return 0; //move will be undone (naive implementation)
    }
    return 1;
	
}

void UndoMove(S_BOARD *pos) {
    int hisPly = pos->hisPly-1;
    // restore old castle rights, old enpas square old fifty move
    
    //ASSERT(CheckBoard(pos));
	ASSERT(GeneratePosKey(pos)==pos->posKey);
    //remove old HASH
	if(pos->enPas != NO_SQ) HASH_EP;
    HASH_CA;

    pos->fiftyMove = pos->history[hisPly].fiftyMove;
    pos->enPas = pos->history[hisPly].enPas;
    pos->castlePerm = pos->history[hisPly].castlePerm;

    //add new HASH
	if(pos->enPas != NO_SQ) HASH_EP;
    HASH_CA;

    int move = pos->history[hisPly].move; 

    int from = FROMSQ(move); // korrekt??
    int to = TOSQ(move); //korrekt??
    int prompce = PROMOTED(move); // TODO korrekt??
    int enpas = ENPAS(move); //ENPAS(move);
    int castle = CASTLE(move);
    int movedpc = pos->pieces64[to];

    int cappc = CAPTURED(move); //pos->pieces64[from];
    int cap = CAPTURED(move);

    int side = !pos->side; 

    
    //Case 1: normal move to empty square or pawnstart
    if(!cap && !enpas && !castle && !prompce){
        // Move piece
        //printf("case 1\n");
        MovePiece(to, from, pos);
    } 
    
    //Case 2: normal capture move (cap)
    if(cap && !enpas && !prompce){
        //printf("case 2\n");
        // move piece back
        MovePiece(to, from, pos);
        // readd piece 
        AddPiece(to, pos, cap);
    } 
    
    //Case 3: enpassant capture move (cap && enpas) (enpas implies cap)
    if(enpas){
        //printf("case 3\n");
        int dir = side ? N : S;
        // move piece from enpas square
        MovePiece(to,from,pos); //to == enpas square
        int ep = side ? wp : bp;
        // readd enemy pawn at weird place
        AddPiece(to+dir,pos,ep);
    } 
    
    //Case 4: castle (castle)
    if(castle){
        //printf("case 4\n");
        // move king back
        MovePiece(to,from,pos);
        int ROOKRANK = side ? RANK_8 : RANK_1;
        if(FILESQ64(to) == FILE_G){
            // move rook from F,rookrank to H
            MovePiece(FR2SQ64(FILE_F,ROOKRANK),FR2SQ64(FILE_H,ROOKRANK),pos);
        }
        else if(FILESQ64(to) == FILE_C){
            // move rook from D,rookrank to A
            MovePiece(FR2SQ64(FILE_D,ROOKRANK),FR2SQ64(FILE_A,ROOKRANK),pos);
        }
    } 
    
    //Case 5: promotion but no capture
    if(!cap && prompce){
        //printf("case 5\n");
        int p = side ? bp : wp;
        //readd pawn
        AddPiece(from,pos,p);
        //remove new piece
        RemovePiece(to,pos);
    } 
    //Case 6: promotion with capture
    if(cap && prompce){
        //printf("case 6\n");
        int p = side ? bp : wp;
        //readd pawn
        AddPiece(from,pos,p);
        //remove new piece
        RemovePiece(to,pos);
        //readd captured piece
        AddPiece(to,pos,cap);
    } 

    pos->side = !pos->side;
    HASH_SIDE;
    pos->hisPly--;
    pos->ply--;
    //printmv(move);
    //printf("fails undo? \n");
	//ASSERT(GeneratePosKey(pos)==pos->posKey);
}

void MakeNullMove(S_BOARD* pos){
    
    pos->ply++;

    if(pos->enPas != NO_SQ) HASH_EP;

    pos->history[pos->hisPly].move = 0;
    pos->history[pos->hisPly].fiftyMove = pos->fiftyMove;
    pos->history[pos->hisPly].enPas = pos->enPas;
    pos->history[pos->hisPly].castlePerm = pos->castlePerm;

    pos->enPas = NO_SQ;

    pos->side  = !pos->side;
    HASH_SIDE;

    pos->hisPly++;

    return;
}

void UndoNullMove(S_BOARD* pos){
    
    pos->ply--;
    pos->hisPly--;

    if(pos->enPas != NO_SQ) HASH_EP;

    pos->castlePerm  = pos->history[pos->hisPly].castlePerm;
    pos->fiftyMove  = pos->history[pos->hisPly].fiftyMove;
    pos->enPas  = pos->history[pos->hisPly].enPas;

    if(pos->enPas != NO_SQ) HASH_EP;

    pos->side  = !pos->side;
    HASH_SIDE;

    return;
}
