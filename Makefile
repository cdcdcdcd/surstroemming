
NAME = surströmming
DISNAME = surströmming


all:
	g++ -DNAME=\"$(DISNAME)\" -o $(NAME) main.cpp init.cpp bitboards.cpp board.cpp attack.cpp moves.cpp data.c io.cpp makemove.cpp ai.cpp input.cpp perft.cpp evaluate.c misc.c hashkey.c pvtable.c polykeys.c polybook.c gui.c uci.c -lSDL2 -lSDL2_image -ldl 

$(NAME):
	rm $(NAME)
