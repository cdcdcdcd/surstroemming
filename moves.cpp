#include "defs.hpp"
#include "stdio.h"

const int VictimScore[13] = { 0, 100, 200, 300, 400, 500, 600, 100, 200, 300, 400, 500, 600 };
static int MvvLvaScores[13][13];

void InitMvvLva() {
	int Attacker;
	int Victim;
	for(Attacker = wp; Attacker <= bk; ++Attacker) {
		for(Victim = wp; Victim <= bk; ++Victim) {
			MvvLvaScores[Victim][Attacker] = VictimScore[Victim] + 6 - ( VictimScore[Attacker] / 100);
		}
	}		
}

void AddPawn(const S_BOARD *pos, S_MOVELIST *list,int from,int to,int topiece,int flags){ //TODO to and topiece redundant
    ASSERT(!ISOFFBOARD(from));
    ASSERT(!ISOFFBOARD(to));
    int move;
    int side = pos->side; 
    int SECONDLASTRANK = side ? RANK_2 : RANK_7;
    int prom;
    if(RANKSQ64(from) == SECONDLASTRANK){
        for(int i = 0; i < 4; i++){
            prom = prompce[side][i];
            move = MVF(from, to, topiece, prom,flags);
            list->moves[list->count].move = move; 
            list->moves[list->count].score = 0; 
            list->count++;
        }
    }
    else{
        move = MVF(from, to, topiece, 0,flags);
        list->moves[list->count].move = move; 
        if(!ENPAS(flags)&&move&FLAGCAP) list->moves[list->count].score = MvvLvaScores[topiece][pos->pieces64[from]]+INF;
        else if(!(move&FLAGCAP)) list->moves[list->count].score = 0;
        else list->moves[list->count].score = 105+INF; //enpas score
        list->count++;
    }
}

void AddMove(const S_BOARD *pos, S_MOVELIST *list,int from,int to,int topiece){ //to and topiece redundant
    int move;
    int side = pos->side; 
    move = MVF(from, to, topiece, 0,0); 
    list->moves[list->count].move = move; 
    if(move & FLAGCAP){
        list->moves[list->count].score = MvvLvaScores[topiece][pos->pieces64[from]];
    }
    else{
        if(pos->searchKill[0][pos->ply] == move){
            list->moves[list->count].score = 50+INF;
        }else if(pos->searchKill[1][pos->ply] == move){
            list->moves[list->count].score = 49+INF;
        }
        else{
            list->moves[list->count].score = pos->searchHist[pos->pieces64[FROMSQ(move)]][TOSQ(move)];
        }
    }
    
    list->count++;
}

void AddCastleMove(const S_BOARD *pos, S_MOVELIST *list,int KC){ //to and topiece redundant
    int move;
    int side = pos->side; 
    int toRANK = side ? RANK_8 : RANK_1;
    int toFILE = KC ? FILE_G : FILE_C;
    int to = FR2SQ64(toFILE,toRANK);
    int from = FR2SQ64(FILE_E,toRANK);
    move = MVF(from, to, EMPTY, 0, FLAGCSTL);
    list->moves[list->count].move = move; 
    list->moves[list->count].score = 1; //TODO value of piece
    list->count++;
}


//bit
inline U64 getRayAttacks(U64 occupied, int dir8, int square) {
   U64 attacks = rayAttacks[dir8][square];
   U64 blocker = attacks & occupied;
   if ( blocker ) {
      square = dirs[dir8] < 0 ? msb(blocker) : lsb(blocker); //TODO maybe more efficient way
      attacks ^= rayAttacks[dir8][square];
   }
   return attacks;
}

inline U64 GetRookAttacks(int sq, U64 occupancy) {
    return getRayAttacks(occupancy,Ni,sq) | getRayAttacks(occupancy,Si,sq) | getRayAttacks(occupancy,Ei,sq) | getRayAttacks(occupancy,Wi,sq);
}

inline U64 GetBishopAttacks(int sq, U64 occupancy) {
    return getRayAttacks(occupancy,NEi,sq) | getRayAttacks(occupancy,SEi,sq) | getRayAttacks(occupancy,SWi,sq) | getRayAttacks(occupancy,NWi,sq);
}

inline U64 GetQueenAttacks(int sq, U64 occupancy) {
    return GetBishopAttacks(sq, occupancy) | GetRookAttacks(sq, occupancy);
}

#define GetKnightAttacks(sq) (KnightAttacks[sq])
#define GetKingAttacks(sq) (KingAttacks[sq])
#define GetPawnAttacks(sq,col) (PawnAttacks[col][sq])
#define GetPawnQuiet(sq,col) (PawnQuiet[col][sq])
#define GetPawnBlock(sq,col) (PawnBlock[col][sq])

inline U64 GetPieceAttacks(int sq, U64 occupancy, int piece) {
    switch (piece) {
        case wn:
        case bn:
            return GetKnightAttacks(sq);
        case wb:
        case bb:
            return GetBishopAttacks(sq, occupancy);
        case wr:
        case br:
            return GetRookAttacks(sq, occupancy);
        case wq:
        case bq:
            return GetQueenAttacks(sq, occupancy);
        case wk:
        case bk:
            return GetKingAttacks(sq);
    }
    return 0; //error
}

int BitSqAttacked(const int sq, const int side, const S_BOARD *pos){

    int n = side ? wn : bn;
    // Knight
    if(pos->piecebb[n] & GetKnightAttacks(sq)){
        return True;
    }
    int k = side ? wk: bk;
    // King
    if(pos->piecebb[k] & GetKingAttacks(sq)){
        return True;
    }
    int r = side ? wr : br;
    int q = side ? wq : bq;
    // Rook
    if((pos->piecebb[r] | pos->piecebb[q]) & GetRookAttacks(sq,pos->occu[BOTH])){
        return True;
    }
    int b = side ? wb: bb;
    // Bishop
    if((pos->piecebb[b] | pos->piecebb[q]) & GetBishopAttacks(sq,pos->occu[BOTH])){
        return True;
    }
    // pawn
    int p = side ? wp: bp;
    if(pos->piecebb[p] & GetPawnAttacks(sq,side)){ 
        return True;
    }

    return False;
}
void GenerateCastleMoves(S_MOVELIST* list, S_BOARD* pos) {
    int side = pos->side;
    int FIRSTRANK = side ? RANK_8 : RANK_1;
    int QC = side ? pos->castlePerm & BQC : pos->castlePerm & WQC;
    int KC = side ? pos->castlePerm & BKC : pos->castlePerm & WKC;
    int Fsq = FR2SQ64(FILE_F,FIRSTRANK);
    int Gsq = FR2SQ64(FILE_G,FIRSTRANK);
    int Csq = FR2SQ64(FILE_C,FIRSTRANK);
    int Bsq = FR2SQ64(FILE_B,FIRSTRANK);
    int Dsq = FR2SQ64(FILE_D,FIRSTRANK);
    int Esq = FR2SQ64(FILE_E,FIRSTRANK);
    if(!pos->pieces64[Fsq] && !pos->pieces64[Gsq] && !BitSqAttacked(Fsq,side,pos) && !BitSqAttacked(Gsq,side,pos) && !BitSqAttacked(Esq,side,pos)&& KC)
        AddCastleMove(pos,list,1);
    if(!pos->pieces64[Bsq] && !pos->pieces64[Csq] && !pos->pieces64[Dsq] && !BitSqAttacked(Csq,side,pos) && !BitSqAttacked(Dsq,side,pos) && !BitSqAttacked(Esq,side,pos) && QC)
        AddCastleMove(pos,list,0);

    //if(pos->side == WHITE){
    //    if(pos->castlePerm & WKC){
    //        //CHECK if F1 G1 
    //        if(((~pos->occu[BOTH]&(1ULL << f1))&&(~pos->occu[BOTH]&(1ULL << g1)))){
    //            if(!BitSqAttacked(f1,pos->side,pos) && !BitSqAttacked(g1,pos->side,pos)&& !BitSqAttacked(e1,pos->side,pos))
    //                AddCastleMove(pos, list, True);
    //        }
    //    }
    //    if(pos->castlePerm &  WQC){
    //        //CHECK if B1 C1 D1 
    //        if( (~pos->occu[BOTH]&(1ULL << b1)) && (~pos->occu[BOTH]&(1ULL << c1) && (~pos->occu[BOTH]&(1ULL << d1))) ){
    //            if(!BitSqAttacked(b1,pos->side,pos) && !BitSqAttacked(c1,pos->side^1,pos) && !BitSqAttacked(d1,pos->side,pos) && !BitSqAttacked(e1,pos->side,pos))
    //                AddCastleMove(pos, list, False);
    //        }
    //    }
    //}
    //else{
    //    if(pos->castlePerm & BKC){
    //        //CHECK if F8 G8 E8 (king check)
    //        if(((~pos->occu[BOTH]&(1ULL << f8))&&(~pos->occu[BOTH]&(1ULL << g8)))){
    //            if(!BitSqAttacked(f8,pos->side,pos) && !BitSqAttacked(g8,pos->side,pos)&& !BitSqAttacked(e8,pos->side,pos))
    //                AddCastleMove(pos, list, True);
    //        }
    //    }
    //    if(pos->castlePerm & BQC){
    //        //CHECK if B8 C8 D8
    //        if((~pos->occu[BOTH]&(1ULL << b8))&&(~pos->occu[BOTH]&(1ULL << c8)&&(~pos->occu[BOTH]&(1ULL << d8)))){
    //            if(!BitSqAttacked(b8,pos->side,pos) && !BitSqAttacked(c8,pos->side,pos) && !BitSqAttacked(d8,pos->side,pos)&& !BitSqAttacked(e8,pos->side,pos))
    //                AddCastleMove(pos, list, False);
    //        }
    //    }
    //}
}

//get piecem move
void GeneratePieceMoves(S_MOVELIST* list, U64 movers, S_BOARD* pos, int piece) {
    int xside = pos->side ^ 1;

    //printf("movers\n");
    //PrintBitBoard(movers);
    while (movers) {
        int from = popAndGetLsb(&movers);

        U64 targets = GetPieceAttacks(from, pos->occu[BOTH], piece) & ~pos->occu[pos->side];
        //PrintBitBoard(targets);
        while (targets) {
            int to = popAndGetLsb(&targets);//TODO POP
            int topiece = pos->pieces64[to];
            AddMove(pos, list,from,to,topiece); //TODO make move index 64
        }
    }
}

void GeneratePieceAttackOnly(S_MOVELIST* list, U64 movers, S_BOARD* pos, int piece) {
    int xside = pos->side ^ 1;

    while (movers) {
        int from = popAndGetLsb(&movers);

        U64 targets = GetPieceAttacks(from, pos->occu[BOTH], piece) & pos->occu[xside];
        while (targets) {
            int to = popAndGetLsb(&targets);//TODO POP
            int topiece = pos->pieces64[to];
            AddMove(pos, list,from,to,topiece); //TODO make move index 64
        }
    }
}


void GeneratePawnMoves(S_MOVELIST* list, U64 movers, S_BOARD* pos){
    int xside = pos->side ^ 1; 
    int p = pos->side ? wp : bp; 

    while(movers){
        int from = popAndGetLsb(&movers);
        U64 enpas;
        if(!(GetPawnBlock(from,pos->side) & pos->occu[BOTH])){//has blocker
            U64 quiet = GetPawnQuiet(from,pos->side) & ~pos->occu[BOTH];
            while (quiet) {
                int to = popAndGetLsb(&quiet);//TODO POP
                int topiece = pos->pieces64[to];
                int flag = ((from - to == N+N) || (from -to == S+S)) ? FLAGPWNSSTRT : 0;
                AddPawn(pos,list,from,to,topiece, flag);//pawn start or not
            }
        }

        if(pos->enPas != NO_SQ) enpas = (1ULL << pos->enPas); 
        else enpas = 0;
        
        U64 targets = (GetPawnAttacks(from,pos->side) & (pos->occu[xside] | enpas) );

        while (targets) {
            int to = popAndGetLsb(&targets);//TODO POP
            int topiece = pos->pieces64[to];
            int flag = (to == pos->enPas) ? FLAGENPAS : 0;
            AddPawn(pos,list,from,to,topiece, flag);//enpas, pawn start
        }
    }
    
}

void GeneratePawnAttacksOnly(S_MOVELIST* list, U64 movers, S_BOARD* pos){

    //U64 pawnAttacks = pos->side == WHITE ? ShiftNW(wp) | ShiftNE(wp)
     //                                 : ShiftSW(bp) | ShiftSE(bp);


    int xside = pos->side ^ 1; 
    int p = pos->side ? wp : bp; 

    while(movers){
        int from = popAndGetLsb(&movers);
        U64 enpas;

        if(pos->enPas != NO_SQ) enpas = (1ULL << pos->enPas); 
        else enpas = 0;
        
        U64 targets = (GetPawnAttacks(from,pos->side) & (pos->occu[xside] | enpas) );

        while (targets) {
            int to = popAndGetLsb(&targets);//TODO POP
            int topiece = pos->pieces64[to];
            int flag = (to == pos->enPas) ? FLAGENPAS : 0;
            AddPawn(pos,list,from,to,topiece, flag);//enpas, pawn start
        }
    }
    
}
void GenerateAllMoves(S_MOVELIST* list, S_BOARD* pos){
    if(pos->side == WHITE){
        for(int pce = wp;pce <= wk;pce++){
            if(pce == wp) GeneratePawnMoves(list,pos->piecebb[pce],pos);
            else GeneratePieceMoves(list,pos->piecebb[pce],pos,pce);
        }
    }
    else{
        for(int pce = bp;pce <= bk;pce++){
            if(pce == bp) GeneratePawnMoves(list,pos->piecebb[pce],pos);
            else GeneratePieceMoves(list,pos->piecebb[pce],pos,pce);
        }
    }
    GenerateCastleMoves(list, pos);
}

void GenerateCaps(S_MOVELIST* list, S_BOARD* pos){
    if(pos->side == WHITE){
        for(int pce = wp;pce <= wk;pce++){
            if(pce == wp) GeneratePawnAttacksOnly(list,pos->piecebb[pce],pos);
            else GeneratePieceAttackOnly(list,pos->piecebb[pce],pos,pce);
        }
    }
    else{
        for(int pce = bp;pce <= bk;pce++){
            if(pce == bp) GeneratePawnAttacksOnly(list,pos->piecebb[pce],pos);
            else GeneratePieceAttackOnly(list,pos->piecebb[pce],pos,pce);
        }
    }
}
