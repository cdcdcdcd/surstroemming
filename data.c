#include "defs.hpp"


const char printpce[] = ".PNBRQKpnbrqk";
char SideChar[] = "wb-";

int isBig[13] = { 0,0,1,1,1,1,1,0,1,1,1,1,1}; //nonpawn
int isMaj[13] = { 0,0,0,0,1,1,1,0,0,0,1,1,1};
int isMin[13] = { 0,0,1,1,0,0,0,0,1,1,0,0,0};
int valList[13] = { 0, 100, 350, 350, 525, 1000, 50000, 100, 350, 350, 525, 1000, 50000}; 
int colList[13] = { BOTH, WHITE, WHITE, WHITE, WHITE, WHITE, WHITE, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK};
int prompce[2][4] = {{wq,wn,wr,wb},{bq,bn,br,bb}};

const int NDir[8] = { -8, -19, -21, -12, 8, 19, 21, 12 };
const int RDir[4] = { -1, -10, 1, 10 };
const int BDir[4] = { -9, -11, 11, 9};
const int KDir[8] = { -1, -10, 1, 10, -9, -11, 11, 9 };
const int PDir[][3] = {{9,11,10},{-9,-11,10}}; // TODO make this a 2 d array with per color instead {{9,11},{-9,-11}}
                                               // TODO unused? buggy?
